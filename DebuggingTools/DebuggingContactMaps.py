#!/usr/bin/env python
# coding: utf-8

# In[3]:


import glob
import os
import random
import matplotlib.pyplot as plt
import numpy as np
from predictingcontactmaps.Utils.KerasGeneratorBinary import Voxel_to_Contact_Generator
import pickle

# In[2]:


#update params to reflect constructor for Voxel_to_Contact_Generator
params = {'dim': (32,32,32),
            'batch_size': 64,
            'n_channels': 1,
            'shuffle': True,
            'margin': 1,
            'voxel_size': 2,
            'n_augmentations': 3,
            'augment': True}

#build list [[antibody, antigen, full complex], [antibody, antigen, full complex], ...]
ab_ag_files = []

antibodies = glob.glob("/home/sdebesai/predictingcontactmaps/Data/split_Ab_Ag_size_filtered/*Ab.pdb")

start_id = 0
for ind, ab in enumerate(antibodies):
    if ab == '/home/sdebesai/predictingcontactmaps/Data/split_Ab_Ag_size_filtered/1E4X_2_Ab.pdb':
        start_id = ind
    ag = ab.replace('_Ab.pdb','_Ag.pdb')
    both = ab.replace('_Ab.pdb','.pdb')
    both = both.replace('split_Ab_Ag_size_filtered','merged_Ab_Ag')
    ab_ag_files.append([ab, ag, both])

infile = open('/home/sdebesai/predictingcontactmaps/Utils/train_data_list_size_filtered', 'rb')
train_data_list = pickle.load(infile)
infile.close()

#split list into training and validation data and store in dictionary
#end_id = start_id + 10
#partition = {}
#randomly select data for training & validation
#rn 75/25 train-test split but can change
#num_files_test = int(len(ab_ag_files)/10)
#random.shuffle(ab_ag_files)

#partition.update({'debug': ab_ag_files[start_id-1:end_id]})


#Generators
#training_generator = Voxel_to_Contact_Generator(file_list=partition['debug'], **params)

#data = training_generator.__getitem__(0)
#data_flat = data[1].flatten()

np.random.seed(91106)
full_len = len(train_data_list)
eval_size = full_len // 4
test_size = full_len // 2
test_indices = np.random.choice(np.array(range(full_len)), test_size, replace=False)
test_indicies = sorted(test_indices, reverse=True)

test_fold = []
eval_set = []

print(test_indices)
print("there are", full_len)

for ind in test_indices[:-eval_size]:
    print(ind)
    test_fold.append(train_data_list.pop(ind))

for ind in test_indices[-eval_size:]:
    print(ind)
    eval_set.append(train_data_list.pop(ind))

eval_generator = Voxel_to_Contact_Generator(eval_set, n_channels=1, n_augmentations=3, batch_size = 9, augment=True, dims = (32, 32, 32), voxiel_size=1.,check_margin=0.5, shuffle=False, large_background=False, eval_set=True)

eval_data = training_generator.__getitem__(0)
# In[6]:


#print(data[0][1].sum())


# In[16]:


#print(data[0][0].sum())


# In[15]:

contact_map = data[1][0]
np.where(contact_map < 8.0, 1, 0)
print(contact_map.sum())
#print(data[1].sum())


# In[ ]:



