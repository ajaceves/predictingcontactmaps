#siamese network wikipedia page: https://en.wikipedia.org/wiki/Siamese_neural_network
#original paper: https://www.cs.cmu.edu/~rsalakhu/papers/oneshot1.pdf

import glob, os, csv, random
from copy import deepcopy

#Third party import
import argparse
import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.layers import Conv3D, MaxPooling3D, Flatten, Dropout, Input, Dense, Activation, Lambda
from keras.models import Model
from keras.losses import binary_crossentropy
from keras.callbacks import EarlyStopping
from keras.optimizers import Adam
from keras import backend as K

#Custom modules
from MoleculeCompletion.Utils.BasicPDBGenerator import SiamesePDBStreamer

#the following fn is the cannonical loss function for a siamese network:
def contrastive_loss(y_true, y_pred):
    '''Contrastive loss from Hadsell-et-al.'06
    http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    
    as a blog: https://towardsdatascience.com/contrastive-loss-for-supervised-classification-224ae35692e7
    '''
    margin = 1
    sqaure_pred = K.square(y_pred)
    margin_square = K.square(K.maximum(margin - y_pred, 0))
    return K.mean(y_true * sqaure_pred + (1 - y_true) * margin_square)

#these two functions are functions we will use in the lambda layer of the network: https://www.tensorflow.org/api_docs/python/tf/keras/layers/Lambda
def euclidean_distance(vects):
    x, y = vects
    sum_square = K.sum(K.square(x - y), axis=1, keepdims=True)
    return K.sqrt(K.maximum(sum_square, K.epsilon()))

def eucl_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)


def main():
    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--data_directory', type=str, help = 'directory to read structures from', default='/home/aiden/Documents/Mayo/SSL/datasets/eSOL/structures/')
    parser.add_argument('--key', type=str, help = 'custom key, overwrites automatic detection from structure directory', default=None)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 100)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 9)
    parser.add_argument('--n_aug', type=int, help = 'The number of augmentations to make to each piece of training data', default = 3)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0001)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.1)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 128)
    parser.add_argument('--augment', help = 'Whether or not to augment data. If n_aug > 1, this is overwritten to True', default = True)
    parser.add_argument('--check_size', help = 'Whether or not to check if all of the ligands fit within the voxel grids', default = True)
    parser.add_argument('--vox_size', type=float, help = 'the size in angstroms of each voxel', default = 2.)
    parser.add_argument('--dims', help = 'the length of one side of the overall voxel grid, in voxel units (a cube)', default = 32 )
    parser.add_argument('--margin', help = 'the margin (in angstroms) by which pdbs must fit inside the overall voxel grid', default = 0.5)
    #the siamese network compares two inputs at a time:
    parser.add_argument('--number_pairs', help = 'how many pairs of inputs to generate for each entry in the training list', default = 1)

    args = parser.parse_args()
    data_dir = args.data_directory
    key = args.key
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    n_aug = int(args.n_aug)
    learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    n_filters = int(args.n_filters)
    aug = args.augment
    check_size = args.check_size
    check_margin = args.margin
    vox_size = args.vox_size
    dims = args.dims
    dims = (dims,dims,dims)
    num_pairs = args.number_pairs

    data_dir = data_dir.rstrip('/') + str('/*.pdb')
    if key is None:
        key = data_dir.rstrip('/*.pdb') + str('/*.csv')
        key = glob.glob(key)
        if len(key) > 1:
            raise ValueError("Found more than one potential key file. Consider specifying a key manually")
        key = key[0]
        
    data_list = glob.glob(data_dir)
    print("Found %i structures" %len(data_list))
    print("Using key: " +str(key))

    #lookup structure names against key
    y_dict = {}
    with open(key, encoding="utf8", errors='ignore', mode='r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for rows in csv_reader:
            y_dict[rows[0]] = float(rows[1])
    
    train_data_list = []
    for entries in data_list:
        base = os.path.basename(entries)
        structure_name = os.path.splitext(base)[0]
        #try / except is evil. don't do this
        try:
            try:
                y_value = y_dict[structure_name]
                train_data_list.append([entries, y_value])
            except:
                y_value = y_dict[structure_name.upper()]
                train_data_list.append([entries, y_value])
        except:
            continue

    keys_not_found = len(data_list) - len(train_data_list)
    print('Excluded %d files which were not found in the key.' % int(keys_not_found))

    #Setup Callbacks. Callbacks are a type of function that does some action at the end of each epoch of training. This one stops the training...
    #   if the validation loss does not decrease by 0.001 from the running best after 10 epochs
    ES = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=10, verbose=1, mode='min', restore_best_weights = True)

    #This is the keras "functional api": https://keras.io/guides/functional_api/

    #typically networks only have a single input, but the siamese network takes in two tensors at a time:
    struct_a_input = Input(shape=(32,32,32,7), name='Struct_a_in')
    struct_b_input = Input(shape=(32,32,32,7), name='Struct_b_in')

    #BEGIN SIAMESE "BRANCH": https://bit.ly/2Yz0ouO
    S_in = Input(shape=(32,32,32,7), name='S_in_shared')
    S = Conv3D(filters = n_filters, kernel_size = 4, activation='relu', padding='same', data_format="channels_last")(S_in)
    S = MaxPooling3D(padding = "same")(S)
    S = Conv3D(filters = n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    S = MaxPooling3D(padding = "same")(S)
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S = MaxPooling3D(padding = "same")(S)
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S = Flatten()(S)
    S_out = Dense(1024, activation="linear")(S)
    #END SIAMESE BRANCH

    #We declare the block above to be a "submodel", so that we can reuse it easily:
    Siamese_Branch = Model(inputs=[S_in],outputs=[S_out])
    #Print a summary of the branch to the console
    Siamese_Branch.summary()

    #make 1st copy of the branch submodel we delated above, using struct_a_input
    struct_a_branch = Siamese_Branch(struct_a_input)
    #make 2nd copy of the branch submodel we delated above, using struct_b_input
    struct_b_branch = Siamese_Branch(struct_b_input)
    
    #the keras lambda layer uses the same syntax of other layers to specify its input - the name of the input layer is in the final set of parethesis
    #in this case the input is a LIST of two inputs
    #the lamda layer applies the euclidian distance fn we wrote above to the two inputs
    final_output = Lambda(euclidean_distance, output_shape=eucl_dist_output_shape)([struct_a_branch, struct_b_branch])
    
    #declare the overall model: it starts with the two seperate inputs and ends with the output of the lambda layer
    #the submodel (Siamese_branch) is wholly encapsulated within this model
    main_model = Model(inputs=[struct_a_input,struct_b_input], outputs=[final_output])
    #specify our optimizer
    adam = Adam(lr=learning_rate)
    #compile the model, using the custom loss function we wrote above and a useful metric:
    #metric vs loss fn: metric is not directly optimized, loss fn is. can specify multiple metrics and multiple loss fns!
    main_model.compile(optimizer=adam, loss=contrastive_loss, metrics=[tf.keras.metrics.AUC()])
    #print summary of the complete model. note that there is a single line for the entire Siamese_branch submodel
    main_model.summary()
    
    #to make test fold, set seed and shuffle in reproducible way
    np.random.seed(91106)    
    full_len = len(train_data_list)
    test_size = full_len // 10
    test_indicies = np.random.choice(np.array(range(full_len)),test_size,replace=False)
    test_indicies = sorted(test_indicies,reverse=True)
    
    test_fold = []
    for ind in test_indicies:
        test_fold.append(train_data_list.pop(ind))

    print("Training set length: %i" %len(train_data_list))
    print("Test set length: %i" %len(test_fold))

    #setup a seperate keras generator for each of the training and test sets:
    train_streamer = SiamesePDBStreamer(train_data_list, n_augmentations = n_aug, batch_size=batch_size, augment = aug, dim = dims, voxel_size = vox_size, check_sizes = check_size, margin = check_margin, number_pairs = num_pairs)
        
    test_streamer = SiamesePDBStreamer(test_fold, n_augmentations = n_aug, batch_size = 8, augment = aug, dim = dims, voxel_size = vox_size, check_sizes = check_size, margin = check_margin, number_pairs = num_pairs)
    
    #fit the model, use the validation streamer:
    main_model.fit(train_streamer, validation_data = test_streamer, epochs=epochs, shuffle = True, workers = 1, max_queue_size = 10, use_multiprocessing=False, callbacks=[ES])
    
if __name__ == "__main__":
    main()
