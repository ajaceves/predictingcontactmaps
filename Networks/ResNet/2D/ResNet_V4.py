#siamese network wikipedia page: https://en.wikipedia.org/wiki/Siamese_neural_network
#original paper: https://www.cs.cmu.edu/~rsalakhu/papers/oneshot1.pdf
'''
ResNet v1. Fix the minimum number of branch layers to 3 and use fewer main layers.
1. loss
2. optimizer
3. learning rate
4. layers in branches
5. layers in main part of network
7. starting number of filters
'''
import glob, os, csv, random
from datetime import datetime
from copy import deepcopy
import pickle 
#Third party import
import argparse
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv3D, AveragePooling2D, Flatten, Dropout, Input, Dense, Reshape, Conv2D, BatchNormalization, Conv3DTranspose
from tensorflow.keras.layers import Activation, Lambda, Add, MaxPooling3D, UpSampling2D, UpSampling3D
from tensorflow.keras.models import Model
from tensorflow.keras.losses import binary_crossentropy, MSE, MAE, MSLE, KLDivergence
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import SGD
#for callbacks
from tensorflow.keras.utils import GeneratorEnqueuer, Sequence, OrderedEnqueuer
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard, Callback
#for tuning
import kerastuner as kt
from kerastuner.engine.hypermodel import HyperModel
from kerastuner.engine.hyperparameters import HyperParameters
import datetime

#Custom modules
from predictingcontactmaps.Utils.KerasGenerator import Voxel_to_Contact_Generator
from predictingcontactmaps.Utils.ColorizeGreyscaleContactMaps import colorize

parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
parser.add_argument('--data_directory', type=str, help = 'directory to read structures from', default='/home/sdebesai/predictingcontactmaps/Data/split_Ab_Ag_size_filtered')
parser.add_argument('--key', type=str, help = 'custom key, overwrites automatic detection from structure directory', default=None)
parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 50)
parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 12)
parser.add_argument('--n_aug', type=int, help = 'The number of augmentations to make to each piece of training data', default = 1)
parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0005)
parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.1)
parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 128)
parser.add_argument('--augment', help = 'Whether or not to augment data. If n_aug > 1, this is overwritten to True', default = True)
parser.add_argument('--check_size', help = 'Whether or not to check if all of the ligands fit within the voxel grids', default = True)
parser.add_argument('--vox_size', type=float, help = 'the size in angstroms of each voxel', default = 1.)
parser.add_argument('--dims', help = 'the length of one side of the overall voxel grid, in voxel units (a cube)', default = 32 )
parser.add_argument('--margin', help = 'the margin (in angstroms) by which pdbs must fit inside the overall voxel grid', default = 0.5)
#the siamese network compares two inputs at a time:
parser.add_argument('--number_channels', help = 'how many channels do we need', default = 1)
parser.add_argument('--large_background', help ='Dkstance to use for null contacts, default is zero', default = False)

args = parser.parse_args()
data_dir = args.data_directory
key = args.key
epochs = int(args.epochs)
batch_size = int(args.batch_size)
n_aug = int(args.n_aug)
learning_rate = float(args.learning_rate)
dropout = float(args.dropout)
n_filters = int(args.n_filters)
aug = args.augment
check_size = args.check_size
check_margin = args.margin
vox_size = args.vox_size
dims = args.dims
dims = (dims,dims,dims)
large_background = args.large_background
n_channels = args.number_channels
    
args = {'data_dir': data_dir, 'key':key, 'epochs':epochs, 'batch_size':batch_size, 'n_aug':n_aug, 'learning_rate':learning_rate,'dropout':dropout, 'n_filters':n_filters,'aug':aug, 'check_size':check_size,'check_margin':check_margin, 'vox_size':vox_size, 'dims':dims, 'n_channels':n_channels}


learning_rate_list = [10*learning_rate,4*learning_rate,2*learning_rate, learning_rate]
loss_list = ['mse', 'mae', 'msle', 'kullback_leibler_divergence']
optimizer_list = ['sgd', 'adam']
activation_list = ['relu', 'sigmoid', 'tanh']    
filters_list = [8, 16, 32]
main_layers_list = [0, 1, 2, 3]
branch_layers_list = [3, 4, 5]

def build_model(hp):
    data_dir = args['data_dir']
    key = args['key']
    epochs=args['epochs']
    batch_size = args['batch_size']
    n_aug = args['n_aug']
    learning_rate = args['learning_rate']
    dropout = args['dropout']
    n_filters = args['n_filters']
    aug = args['aug']
    check_size = args['check_size']
    vox_size = args['vox_size']
    dims = args['dims']
    check_margin = args['check_margin']
    n_channels = args['n_channels']
   
    lr = hp.Choice('learning_rate', values=learning_rate_list)
    loss = hp.Choice('loss', values=loss_list)
    optimizer = hp.Choice('optimizer', values=optimizer_list)
    activation = hp.Choice('activation', values=activation_list)
    filters = hp.Choice('filters', values=filters_list)
    #main_layers = hp.Int('main_layers', min_value=0, max_value=3)
    #branch_layers = hp.Int('branch_layers', min_value=0, max_value=3)
    branch_layers = 1
    main_layers = 3
    
    use_reg = hp.Boolean('use reg')
    dp = hp.Float('dropout', min_value=0.1, max_value=0.9, step=0.2)
    reg = hp.Choice('reg', values=['batch_norm', 'dp'])
    struct_a_input = Input(shape=(32,32,32,8), name='Struct_a_in')
    struct_b_input = Input(shape=(32,32,32,8), name='Struct_b_in')
    #BEGIN SIAMESE "BRANCH": https://bit.ly/2Yz0ouO
    S_in = Input(shape=(32,32,32,8), name='S_in_shared')
    S = Conv3D(filters = filters, kernel_size = 2, activation='relu', padding='same', data_format="channels_last")(S_in)
    S_pool = MaxPooling3D(padding = "same", data_format="channels_last")(S)
    S = Conv3D(filters = filters, kernel_size = 2, activation='relu', padding='same')(S_pool)
    S = Conv3D(filters = filters, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([S_pool, S])  
    
    if use_reg:
        if reg == 'batch_norm':
            S_add = BatchNormalization()(S_add)
        elif reg == 'dp':
            S_add = Dropout(dp)(S_add)
    num_filters = filters
    for i in range(branch_layers):
        num_filters = num_filters*2
        Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
        S_add = Add()([Shortcut, S])
        if use_reg:
            if reg == 'batch_norm':
                S_add = BatchNormalization()(S_add)
            elif reg == 'dp':
                S_add = Dropout(dp)(S_add)

    #END SIAMESE BRANCH
    #TODO: Scale input values
    #TODO: Add better upsampling implementation
    #We declare the block above to be a "submodel", so that we can reuse it easily:
    S_out=S_add
    Siamese_Branch = Model(inputs=[S_in],outputs=[S_out])
    #Print a summary of the branch to the console
    Siamese_Branch.summary()
    #for layers in Siamese_Branch.layers:
     #   print(Siamese_Branch.output_shape)

    #make 1st copy of the branch submodel we delated above, using struct_a_input
    struct_a_branch = Siamese_Branch(struct_a_input)
    #make 2nd copy of the branch submodel we delated above, using struct_b_input
    struct_b_branch = Siamese_Branch(struct_b_input)
    num_filters = num_filters * 2
    S_main_in = Add()([struct_a_branch, struct_b_branch])
    Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_main_in)
    S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S_main_in)
    S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    if use_reg:
        if reg == 'batch_norm':
            S_add = BatchNormalization()(S_add)
        elif reg == 'dp':
            S_add = Dropout(dp)(S_add)
    for i in range(main_layers):
        num_filters = 2*num_filters
        Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
        S = Conv3D(filters =num_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
        S_add = Add()([Shortcut, S])
        if use_reg:
            if reg == 'batch_norm':
                S_add = BatchNormalization()(S_add)
            elif reg == 'dp':
                S_add = Dropout(dp)(S_add)
    S_add = Conv3DTranspose(filters=16, kernel_size=2, strides=(2, 2, 2), padding='same', activation='relu')(S_add) 
    #S_add = Conv3DTranspose(filters=1, kernel_size=2, strides=(2, 2, 2), padding='same', activation='relu')(S_add) 
    #S_add = Conv3DTranspose(filters=1, kernel_size=2, strides=(2, 2, 2), padding='same', activation='relu')(S_add) 
    output_shape = np.array(S_add.shape)[1:]
    print(np.prod(output_shape))
    z =int(np.prod(output_shape)/512/1024) 
    S_add = Reshape((512,1024,z))(S_add)  
    Shortcut = Conv2D(filters = int(filters/4), kernel_size = 1, padding='same')(S_add)
    S = Conv2D(filters = int(filters/4), kernel_size = 4, activation='relu', padding='same')(S_add)
    S = Conv2D(filters = int(filters/4), kernel_size = 4, activation='relu', padding='same')(S)
    S = Add()([Shortcut, S])
    #S = BatchNormalization()(S_add)
    S = Conv2D(1, kernel_size = 1, activation = activation)(S)
    S_final_out = Reshape((512,1024))(S)
    global main_model
    main_model = Model(inputs=[struct_a_input,struct_b_input], outputs=[S_final_out])
    #compile the model, using the custom loss function we wrote above and a useful metric:
    #metric vs loss fn: metric is not directly optimized, loss fn is. can specify multiple metrics and multiple loss fns!
    main_model.summary()
    if optimizer == 'adam':
        optimizer = tf.optimizers.Adam(learning_rate=lr)
    elif optimizer == 'sgd':
        optimizer = tf.optimizers.SGD(learning_rate=lr)
    else:
        raise ValueError("Unexpected optimizer name: %r" % (optimizer,))
    
    main_model.compile(optimizer=optimizer, loss=loss, metrics=['mse'])
    #print summary of the complete model. note that there is a single line for the entire Siamese_branch submodel
    return main_model
    
def main():
    data_dir = args['data_dir']
    key = args['key']
    epochs=args['epochs']
    batch_size = args['batch_size']
    n_aug = args['n_aug']
    learning_rate = args['learning_rate']
    dropout = args['dropout']
    n_filters = args['n_filters']
    aug = args['aug']
    check_size = args['check_size']
    vox_size = args['vox_size']
    dims = args['dims']
    check_margin = args['check_margin']
    n_channels = args['n_channels']

    data_dir = data_dir.rstrip('/') + str('/*.pdb')
    data_list = glob.glob(data_dir)
    """
    if key is None:
        key = data_dir.rstrip('/*.pdb') + str('/*.csv')
        key = glob.glob(key)
        print(key)
        if len(key) > 1:
            raise ValueError("Found more than one potential key file. Consider specifying a key manually")
        key = key[0]
    """    
    #antibodies = glob.glob(data_dir.rstrip('/')+str('/*Ab.pdb'))
    print("Found %i structures" %len(data_list))
    #print("Using key: " +str(key))
    #antibodies.sort() 
    #train_data_list = []
    #for ab in data_list:
        #ag = ab.replace('_Ab.pdb', '_Ag.pdb')
        #both = ab.replace('_Ab.pdb', '.pdb')
        #both = ab.replace('split_Ab_Ag_size_filterd', 'merged_Ab_Ag')
        #train_data_list.append([ab, ag, both])
    infile = open('/home/sdebesai/predictingcontactmaps/Utils/train_data_list', 'rb')
    train_data_list = pickle.load(infile)
    infile.close()
    keys_not_found = len(data_list) - len(train_data_list)
    print('Excluded %d files which were not found in the key.' % int(keys_not_found))
    
    #25% of the data for testing
    np.random.seed(91106)
    full_len = len(train_data_list)
    eval_size = full_len // 4
    cutoff = 0
    
    #to make test fold, set seed and shuffle in reproducible wayi
    test_size = full_len // 2
    test_indicies = np.random.choice(np.array(range(full_len)),test_size,replace=False)
    test_indicies = sorted(test_indicies,reverse=True)
    
    test_fold = []
    
    eval_set = []

    for ind in test_indicies[:-eval_size]:
        test_fold.append(train_data_list.pop(ind))
        
    for ind in test_indicies[-eval_size:]:
        eval_set.append(train_data_list.pop(ind))
    print("Training set length: %i" %len(train_data_list))
    print("Test set length: %i" %len(test_fold))
    print("Eval set length: %i" %len(eval_set))
 
    train_streamer = Voxel_to_Contact_Generator(train_data_list, n_channels=n_channels, n_augmentations =n_aug, batch_size = 8, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)
    test_streamer = Voxel_to_Contact_Generator(test_fold, n_channels=n_channels, n_augmentations = n_aug, batch_size = 8, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)
    eval_streamer = Voxel_to_Contact_Generator(eval_set, n_channels=n_channels, n_augmentations=n_aug, batch_size=8, augment=aug, dim = dims, voxel_size=vox_size, margin=check_margin, large_background=large_background)
    
    #no longer necessary?
    global x_eval
    global y_eval
    x_eval, y_eval = eval_streamer.__getitem__(0)
    num_images = 3
    x_eval = x_eval[0:num_images]
    y_eval = y_eval[0:num_images]
    
    logdir_image = "/central/scratch/sdebesai/logs/image/ResNet/v4/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    # Define the basic TensorBoard callback.
    #tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir_image)
    #file_writer = tf.summary.create_file_writer(logdir_image + '/')

    def log_image(epoch, logs):
      # Use the model to predict the values from the validation dataset.
      global main_model
      global x_eval
      global y_eval
      file_writer = tf.summary.create_file_writer(logdir_image + '/')
      test_pred_raw = main_model.predict(x_eval)

      with file_writer.as_default():
        #images = np.reshape(test_pred_raw, (-1, 512, 1024, 1))
        images = colorize(test_pred_raw)
        tf.summary.image("3 training data examples", images, max_outputs=6, step=epoch)
        #images = np.reshape(y_eval, (-1, 512, 1024, 1))
        images = colorize(y_eval)
        tf.summary.image("3 ground truth examples", images, max_outputs=6, step=epoch)
         
    # Define the per-epoch callback

    #just use default inputs
    IC = keras.callbacks.LambdaCallback(on_epoch_end=log_image)
    
    #Setup Callbacks. Callbacks are a type of function that does some action at the end of each epoch of training. This one stops the training...
    #   if the validation loss does not decrease by 0.001 from the running best after 10 epochs
    ES = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=10, verbose=1, mode='min', restore_best_weights = True)
    print('building model')
    log_dir = "/central/scratch/sdebesai/logs/kerastuner/TB/ResNet/v4" + datetime.datetime.now().strftime("%m%d-%H%M") + '/'
    TB = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1, embeddings_freq=1, write_graph=True, update_freq='batch')
    directory = '/central/scratch/sdebesai/logs/kerastuner/oracle/ResNet/v4/'
    project_name = 'ResNetV4' + datetime.datetime.now().strftime('%m%d-%H%M') 
    tuner = kt.Hyperband(build_model, objective='val_loss', max_epochs=epochs, hyperband_iterations=5, directory=directory, project_name=project_name)
    tuner.search(train_streamer, validation_data = test_streamer, epochs=epochs, callbacks=[ES, TB, IC])
    tuner.search_space_summary()
    best_model = tuner.get_best_models(1)[0]
    best_model.summary()
if __name__ == "__main__":
    main()

