#siamese network wikipedia page: https://en.wikipedia.org/wiki/Siamese_neural_network
#original paper: https://www.cs.cmu.edu/~rsalakhu/papers/oneshot1.pdf

import glob, os, csv, random
from datetime import datetime
from copy import deepcopy

#Third party import
import argparse
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv3D, AveragePooling2D, Flatten, Dropout, Input, Dense, Reshape, Conv2D
from tensorflow.keras.layers import Activation, Lambda, Add, MaxPooling3D, UpSampling2D, UpSampling3D, MaxPooling2D
from tensorflow.keras.models import Model
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import SGD
import pickle

#Custom modules
from predictingcontactmaps.Utils.KerasGeneratorBinary import Voxel_to_Contact_Generator
from predictingcontactmaps.Utils.ColorizeGreyscaleContactMaps import colorize
np.set_printoptions(threshold=np.inf)

#the following fn is the cannonical loss function for a siamese network:
def contrastive_loss(y_true, y_pred):
    '''Contrastive loss from Hadsell-et-al.'06
    http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf
    
    as a blog: https://towardsdatascience.com/contrastive-loss-for-supervised-classification-224ae35692e7
    '''
    margin = 1
    sqaure_pred = K.square(y_pred)
    margin_square = K.square(K.maximum(margin - y_pred, 0))
    return K.mean(y_true * sqaure_pred + (1 - y_true) * margin_square)

#these two functions are functions we will use in the lambda layer of the network: https://www.tensorflow.org/api_docs/python/tf/keras/layers/Lambda
def euclidean_distance(vects):
    x, y = vects
    sum_square = K.sum(K.square(x - y), axis=1, keepdims=True)
    return K.sqrt(K.maximum(sum_square, K.epsilon()))

def eucl_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)

def weights(a):
    cutoff = 8 
    weight_contact = 10
    weight_no_contact = 1
    a = K.less(a, cutoff)
    a = K.cast_to_floatx(a)
    return (a*(weight_contact-weight_no_contact)) + weight_no_contact

def weighted_mse(y_true, y_pred):
    weights_array = weights(y_true)
    diff = K.square(y_pred-y_true)
    weighted_diff = tf.math.multiply(weights_array, diff)
    return K.mean(weighted_diff, axis=-1)

def weighted_mae(y_true, y_pred):
    weights_array = weights(y_true)
    diff = K.abs(y_pred-y_true)
    weighted_diff = tf.math.multiply(weights_array, diff)
    return K.mean(weighted_diff, axis=-1)
    
def main():
    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--data_directory', type=str, help = 'directory to read structures from', default='/home/sdebesai/predictingcontactmaps/Data/split_Ab_Ag_size_filtered')
    parser.add_argument('--key', type=str, help = 'custom key, overwrites automatic detection from structure directory', default=None)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 120)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 12)
    parser.add_argument('--n_aug', type=int, help = 'The number of augmentations to make to each piece of training data', default = 1)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0005)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.1)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 32)
    parser.add_argument('--augment', help = 'Whether or not to augment data. If n_aug > 1, this is overwritten to True', default = True)
    parser.add_argument('--check_size', help = 'Whether or not to check if all of the ligands fit within the voxel grids', default = True)
    parser.add_argument('--vox_size', type=float, help = 'the size in angstroms of each voxel', default = 2.)
    parser.add_argument('--dims', help = 'the length of one side of the overall voxel grid, in voxel units (a cube)', default =32)
    parser.add_argument('--margin', help = 'the margin (in angstroms) by which pdbs must fit inside the overall voxel grid', default = 0.5)
    parser.add_argument('--number_channels', help = 'how many channels do we need', default = 1)
    parser.add_argument('--large_background', help = 'Distance to use for null contacts, default is Zero', default = False)
    parser.add_argument('--loss', help = 'loss fxn', default='binary_crossentropy')
    args = parser.parse_args()
    loss = args.loss
    data_dir = args.data_directory
    key = args.key
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    n_aug = int(args.n_aug)
    learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    n_filters = int(args.n_filters)
    aug = args.augment
    check_size = args.check_size
    check_margin = args.margin
    vox_size = args.vox_size
    dims = args.dims
    dims = (dims,dims,dims)
    large_background = args.large_background
    n_channels = args.number_channels

    data_dir = data_dir.rstrip('/') + str('/*.pdb')
    data_list = glob.glob(data_dir)
    """
    if key is None:
        key = data_dir.rstrip('/*.pdb') + str('/*.csv')
        key = glob.glob(key)
        print(key)
        if len(key) > 1:
            raise ValueError("Found more than one potential key file. Consider specifying a key manually")
        key = key[0]
    """    
    
    infile = open('/home/sdebesai/predictingcontactmaps/Utils/train_data_list_size_filtered', 'rb')
    train_data_list = pickle.load(infile)
    infile.close()
    #antibodies = glob.glob(data_dir.rstrip('/')+str('/*Ab.pdb'))
    print("Found %i structures" %len(data_list))
    #print("Using key: " +str(key))
    
    #antibodies.sort()
    #train_data_list = []
    #for ab in data_list:
        #ag = ab.replace('_Ab.pdb', '_Ag.pdb')
        #both = ab.replace('split_Ab_Ag_size_filterd', 'merged_Ab_Ag')
        #both = both.replace('_Ag.pdb', '.pdb')
        #train_data_list.append([ab, ag, both])
    keys_not_found = len(data_list) - len(train_data_list)
    print('Excluded %d files which were not found in the key.' % int(keys_not_found))

    #Setup Callbacks. Callbacks are a type of function that does some action at the end of each epoch of training. This one stops the training...
    #   if the validation loss does not decrease by 0.001 from the running best after 10 epochs
    ES = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=10, verbose=1, mode='min', restore_best_weights = True)

    #This is the keras "functional api": https://keras.io/guides/functional_api/

    #typically networks only have a single input, but the siamese network takes in two tensors at a time:
    struct_a_input = Input(shape=(32,32,32,8), name='Struct_a_in')
    struct_b_input = Input(shape=(32,32,32,8), name='Struct_b_in')

    #BEGIN SIAMESE "BRANCH": https://bit.ly/2Yz0ouO
    S_in = Input(shape=(32,32,32,8), name='S_in_shared')
    S = Conv3D(filters = n_filters, kernel_size = 4, activation='relu', padding='same', data_format="channels_last")(S_in)
    S_pool = MaxPooling3D(padding = "same", data_format="channels_last")(S)
    S = Conv3D(filters = n_filters, kernel_size = 4, activation='relu', padding='same')(S_pool)
    S = Conv3D(filters = n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    S_add = Add()([S_pool, S])
    #S_add = MaxPooling3D(data_format='channels_last')(S_add)
    Shortcut = Conv3D(filters = n_filters*2, kernel_size = 1, padding='same')(S_add)
    S = Conv3D(filters = n_filters*2, kernel_size = 2, activation='relu', padding='same')(S_add)
    S = Conv3D(filters = n_filters*2, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    #S_add = MaxPooling3D(data_format='channels_last')(S_add)
    Shortcut = Conv3D(filters = n_filters*4, kernel_size = 1, padding='same')(S_add)
    S = Conv3D(filters = n_filters*4, kernel_size = 2, activation='relu', padding='same')(S_add)
    S = Conv3D(filters = n_filters*4, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    #S_add = MaxPooling3D(data_format='channels_last')(S_add)
    Shortcut = Conv3D(filters = n_filters*8, kernel_size = 1, padding='same')(S_add)
    S = Conv3D(filters = n_filters*8, kernel_size = 2, activation='relu', padding='same')(S_add)
    S = Conv3D(filters = n_filters*8, kernel_size = 2, activation='relu', padding='same')(S)
    S_out = Add()([Shortcut, S])
    #END SIAMESE BRANCH
    #TODO: Scale input values
    #TODO: Add better upsampling implementation
    #We declare the block above to be a "submodel", so that we can reuse it easily:
    Siamese_Branch = Model(inputs=[S_in],outputs=[S_out])
    #Print a summary of the branch to the console
    Siamese_Branch.summary()

    #make 1st copy of the branch submodel we delated above, using struct_a_input
    struct_a_branch = Siamese_Branch(struct_a_input)
    #make 2nd copy of the branch submodel we delated above, using struct_b_input
    struct_b_branch = Siamese_Branch(struct_b_input)
    
    S_main_in = Add()([struct_a_branch, struct_b_branch])
    S_main_in = MaxPooling3D(data_format='channels_last')(S_main_in)
    Shortcut = Conv3D(filters = n_filters*4, kernel_size = 1, padding='same')(S_main_in)
    S = Conv3D(filters = n_filters*4, kernel_size = 2, activation='relu', padding='same')(S_main_in)
    S = Conv3D(filters = n_filters*4, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    
    Shortcut = Conv3D(filters = n_filters*8, kernel_size = 1, padding='same')(S_add)
    S = Conv3D(filters = n_filters*8, kernel_size = 2, activation='relu', padding='same')(S_add)
    S = Conv3D(filters = n_filters*8, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])

    Shortcut = Conv3D(filters = n_filters*16, kernel_size = 1, padding='same')(S_main_in)
    S = Conv3D(filters = n_filters*16, kernel_size = 2, activation='relu', padding='same')(S_main_in)
    S = Conv3D(filters = n_filters*16, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    Shortcut = Conv3D(filters = n_filters*32, kernel_size = 1, padding='same')(S_add)
    S = Conv3D(filters = n_filters*32, kernel_size = 2, activation='relu', padding='same')(S_add)
    S = Conv3D(filters = n_filters*32, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    #Shortcut = Conv3D(filters = n_filters*64, kernel_size = 1, padding='same')(S_add)
    #S = Conv3D(filters = n_filters*64, kernel_size = 2, activation='relu', padding='same')(S_add)
    #S = Conv3D(filters = n_filters*64, kernel_size = 2, activation='relu', padding='same')(S)
    #S_add = Add()([Shortcut, S])
    output_dims = np.array(S_add.shape)[1:]
    print(output_dims)
    z = int(np.prod(output_dims)/512/512)
    S = Reshape((512,512,z))(S_add)  
    
    S = MaxPooling2D(data_format='channels_last')(S)
    Shortcut = Conv2D(filters = int(n_filters/4), kernel_size = 1, padding='same')(S)
    S = Conv2D(filters = int(n_filters/4), kernel_size = 4, activation='relu', padding='same')(S)
    S = Conv2D(filters = int(n_filters/4), kernel_size = 4, activation='relu', padding='same')(S)
    S = Add()([Shortcut, S])
    
    Shortcut = Conv2D(filters = int(n_filters/2), kernel_size = 1, padding='same')(S)
    S = Conv2D(filters = int(n_filters/2), kernel_size = 4, activation='relu', padding='same')(S)
    S = Conv2D(filters = int(n_filters/2), kernel_size = 4, activation='relu', padding='same')(S)
    S = Add()([Shortcut, S])
   
    S = MaxPooling2D(data_format='channels_last')(S)    
    Shortcut = Conv2D(filters = int(n_filters), kernel_size = 1, padding='same')(S)
    S = Conv2D(filters = int(n_filters), kernel_size = 4, activation='relu', padding='same')(S)
    S = Conv2D(filters = int(n_filters), kernel_size = 4, activation='relu', padding='same')(S)
    S = Add()([Shortcut, S])

    Shortcut = Conv2D(filters = int(2*n_filters), kernel_size = 1, padding='same')(S)
    S = Conv2D(filters = int(2*n_filters), kernel_size = 4, activation='relu', padding='same')(S)
    S = Conv2D(filters = int(2*n_filters), kernel_size = 4, activation='relu', padding='same')(S)
    S = Add()([Shortcut, S])
    
    S = MaxPooling2D(data_format='channels_last')(S)
    Shortcut = Conv2D(filters = 4*n_filters, kernel_size = 1, padding='same')(S)
    S = Conv2D(filters = 4*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    S = Conv2D(filters = 4*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    S = Add()([Shortcut, S])
   
    Shortcut = Conv2D(filters = 8*n_filters, kernel_size = 1, padding='same')(S)
    S = Conv2D(filters = 8*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    S = Conv2D(filters = 8*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    S = Add()([Shortcut, S])
    
    S = MaxPooling2D(data_format='channels_last')(S)
    Shortcut = Conv2D(filters = 16*n_filters, kernel_size = 1, padding='same')(S)
    S = Conv2D(filters = 16*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    S = Conv2D(filters = 16*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    S = Add()([Shortcut, S])
   
    #Shortcut = Conv2D(filters =32*n_filters, kernel_size = 1, padding='same')(S)
    #S = Conv2D(filters = 32*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    #S = Conv2D(filters =32*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    #S = Add()([Shortcut, S])
    
    #Shortcut = Conv2D(filters = 64*n_filters, kernel_size = 1, padding='same')(S)
    #S = Conv2D(filters = 64*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    #S = Conv2D(filters = 64*n_filters, kernel_size = 4, activation='relu', padding='same')(S)
    #S = Add()([Shortcut, S])
   
    output_dims = np.array(S.shape[1:])
    z = int((np.prod(output_dims)/512/512))
    S = Reshape((512, 512, z))(S)
    S = Conv2D(1, kernel_size = 1, activation = 'sigmoid')(S)
    S_final_out = Reshape((512,512))(S)
    
    main_model = Model(inputs=[struct_a_input,struct_b_input], outputs=[S_final_out])
    #specify our optimizer
    adam = Adam(lr=learning_rate)
    #compile the model, using the custom loss function we wrote above and a useful metric:
    #metric vs loss fn: metric is not directly optimized, loss fn is. can specify multiple metrics and multiple loss fns!
    main_model.compile(optimizer=adam, loss=loss, metrics=[tf.keras.metrics.Accuracy(), tf.keras.metrics.AUC()])
    #print summary of the complete model. note that there is a single line for the entire Siamese_branch submodel
    main_model.summary()
    
    #XXX: THIS IS ONLY FOR DEBUGGING
    eval_size = 3
    cutoff = 0
    
    #to make test fold, set seed and shuffle in reproducible way
    np.random.seed(91106)
    full_len = len(train_data_list)
    test_size = full_len // 10
    test_indicies = np.random.choice(np.array(range(full_len)),test_size,replace=False)
    test_indicies = sorted(test_indicies,reverse=True)
    
    test_fold = []
    eval_set = []
    
    for ind in test_indicies[:-eval_size]:
        test_fold.append(train_data_list.pop(ind))
        
    for ind in test_indicies[-eval_size:]:
        eval_set.append(train_data_list.pop(ind))
        
    print("Training set length: %i" %len(train_data_list))
    print("Test set length: %i" %len(test_fold))
    print("Eval set length: %i" %len(eval_set))
    
    eval_streamer = Voxel_to_Contact_Generator(eval_set, n_channels=n_channels, n_augmentations = 1, batch_size = 3, augment = True, dim = dims, voxel_size = vox_size, margin = check_margin, large_background = large_background)
    
    x_eval, y_eval = eval_streamer.__getitem__(0)
    num_images = 3
    x_eval = x_eval[0:num_images]
    y_eval = y_eval[0:num_images]
    time_stamp = datetime.now().strftime('%Y%m%d-%H%M%S-%f')
    logfile = open(r'/central/scratch/sdebesai/logs/hyperparams/siameseBinary/try3/'+time_stamp, 'w')   
    
    info = ['learning_rate is: ' + str(learning_rate) + '\n', 'loss function is: ' + loss + '\n', 'filters is: ' + str(n_filters)+'\n']
    logfile.writelines(info)
    logfile.close() 
    #print(y_eval.shape)
    #TODO: scale distances!
    logdir_image = "/central/scratch/sdebesai/logs/image/siameseBinary/try3/" + time_stamp
    log_dir = '/central/scratch/sdebesai/logs/kerastuner/TB/siameseBinary/try3/'+ time_stamp + '/'
    # Define the basic TensorBoard callback.
    TB = keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1, embeddings_freq=1, write_graph=True, update_freq='batch')
    file_writer = tf.summary.create_file_writer(logdir_image + '/')
    
    def log_image(epoch, logs):
        # Use the model to predict the values from the validation dataset.
        test_pred_raw = main_model.predict(x_eval)
        with file_writer.as_default():
            images = np.reshape(test_pred_raw, (-1, 512, 512, 1))
            tf.summary.image("3 predicted example results", images, max_outputs=6, step=epoch)
            images = np.reshape(y_eval, (-1, 512, 512, 1))
            tf.summary.image("3 ground truth examples", images, max_outputs=6, step=epoch)

    # Define the per-epoch callback.
    img_callback = keras.callbacks.LambdaCallback(on_epoch_end=log_image)
    
    #setup a seperate keras generator for each of the training and test sets:
    train_streamer = Voxel_to_Contact_Generator(train_data_list,n_channels=n_channels, n_augmentations = n_aug, batch_size=batch_size, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background = large_background)
        
    test_streamer = Voxel_to_Contact_Generator(test_fold, n_channels=n_channels, n_augmentations = n_aug, batch_size = 8, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background = large_background)
    
    #fit the model, use the validation streamer:
    print('Now running fit:')
    main_model.fit(train_streamer, validation_data = test_streamer, epochs=epochs, shuffle = True, workers = 1, max_queue_size = 10, use_multiprocessing=False, callbacks=[ES,TB, img_callback])
    
if __name__ == "__main__":
    main()
