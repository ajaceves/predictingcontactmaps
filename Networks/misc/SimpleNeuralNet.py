import numpy as np
import glob
import os
from KerasGenerator import Voxel_to_Contact_Generator
import random

#update params to reflect constructor for Voxel_to_Contact_Generator
params = {'dim': (32,32,32),
          'batch_size': 64,
          'n_classes': 6,
          'n_channels': 1,
          'shuffle': True,
	  'margin': 1,
	  'voxel_size': 2,
	  'n_augmentations': 3,
	  'augment': True}

#build list [[antibody, antigen, full complex], [antibody, antigen, full complex], ...]
ab_ag_files = []

'''
counter = 0 
whole_complex_files = os.listdir('merged_Ab_Ag')
print(whole_complex_files)
for filename in os.listdir('split_Ab_Ag_size_filtered'):
    #we assume that the antibody is read first then the antigen
    if '_Ab' in filename:
        temp_list = [filename]
        #adds list [antibody] as an element
        ab_ag_files.append(temp_list)
    else:
        #increment the counter
        temp_list = ab_ag_files[counter]
        #updates element to [antibody, antigen]
        temp_list.append(filename)
        #updates element to [antibody, antigen, full complex]
        temp_list.append(whole_complex_files[counter])
        #increment the counter
        counter = counter+1
'''

antibodies = glob.glob("split_Ab_Ag_size_filtered/*Ab.pdb")
for ab in antibodies:
    ag = ab.replace('_Ab.pdb','_Ag.pdb')
    both = ab.replace('_Ab.pdb','.pdb')
    both = both.replace('split_Ab_Ag_size_filtered','merged_Ab_Ag')
    ab_ag_files.append([ab, ag, both])

#split list into training and validation data and store in dictionary 
partition = {}
#randomly select data for training & validation
#rn 50/50 train-test split but can change
num_files_test = int(len(ab_ag_files)/4)
random.shuffle(ab_ag_files)

partition.update({'train': ab_ag_files[:-num_files_test]})
partition.update({'validation': ab_ag_files[-num_files_test:]})

#Generators
training_generator = Voxel_to_Contact_Generator(file_list=partition['train'], **params)
validation_generator = Voxel_to_Contact_Generator(file_list=partition['validation'], **params)

data = training_generator.__getitem__(1)
#print(data[0][0])
#print(data[0][1])
#print(data[1])

