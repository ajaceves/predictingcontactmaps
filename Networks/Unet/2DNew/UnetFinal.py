'''
Unet V1. 2D convolutions. 
Hyperparams for tuning:
1. loss
2. optimizer
3. number of layers in branches
4. "height" of Unet (main layers)
5. learning rate
6. activation fxn
7. dropout 
8. using dropout
9. max/vs avg pooling
10. starting number of filters
'''
import glob, os, csv, random
import datetime
#from datetime import datetime
from copy import deepcopy
import pickle	
#Third party import
import argparse
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import BatchNormalization, Conv3D,Conv2DTranspose, AveragePooling2D, Flatten, Dropout, Input, Dense, Reshape, Conv2D, Conv3DTranspose
from tensorflow.keras.layers import Activation, Lambda, Add, MaxPooling3D,MaxPooling2D, UpSampling2D, UpSampling3D
from tensorflow.keras.models import Model
from tensorflow.keras.losses import binary_crossentropy, MSE, MAE, MSLE, KLDivergence
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.layers import Concatenate, concatenate
import kerastuner as kt
from kerastuner.tuners import Hyperband
from kerastuner.engine.hypermodel import HyperModel
from kerastuner.engine.hyperparameters import HyperParameters
import IPython
import pickle
from tensorflow.keras import backend as K
from sklearn.metrics import f1_score, recall_score, precision_score, confusion_matrix
import itertools
#Custom modules
from predictingcontactmaps.Utils.KerasGenerator3 import Voxel_to_Contact_Generator
from predictingcontactmaps.Utils.ColorizeGreyscaleContactMaps import colorize

def weights(a):
    cutoff = 8 
    weight_contact = 10
    weight_no_contact = 1
    a = K.less(a, cutoff)
    a = K.cast_to_floatx(a)
    return (a*(weight_contact-weight_no_contact)) + weight_no_contact

def weighted_mse(y_true, y_pred):
    weights_array = weights(y_true)
    diff = K.square(y_pred-y_true)
    weighted_diff = tf.math.multiply(weights_array, diff)
    return K.mean(weighted_diff, axis=-1)
def weighted_mae(y_true, y_pred):
    weights_array = weights(y_true)
    diff = K.abs(y_pred-y_true)
    weighted_diff = tf.math.multiply(weights_array, diff)
    return K.mean(weighted_diff, axis=-1)

def toBinary(a):
    cutoff = 8
    a = K.less(a, cutoff)
    a = K.cast_to_floatx(a)
    return a

def binary_accuracy(y_true, y_pred):
    y_pred_binary = toBinary(y_pred)
    y_true_binary = toBinary(y_true)
    return K.mean(K.equal(y_true_binary, K.round(y_pred_binary)), axis=-1)

def main():
    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--data_directory', type=str, help = 'directory to read structures from', default='/home/sdebesai/predictingcontactmaps/Data/split_Ab_Ag_size_filtered')
    parser.add_argument('--key', type=str, help = 'custom key, overwrites automatic detection from structure directory', default=None)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 100)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 12)
    parser.add_argument('--n_aug', type=int, help = 'The number of augmentations to make to each piece of training data', default = 1)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0005)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.1)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 128)
    parser.add_argument('--augment', help = 'Whether or not to augment data. If n_aug > 1, this is overwritten to True', default = True)
    parser.add_argument('--check_size', help = 'Whether or not to check if all of the ligands fit within the voxel grids', default = True)
    parser.add_argument('--vox_size', type=float, help = 'the size in angstroms of each voxel', default = 1.)
    parser.add_argument('--dims', help = 'the length of one side of the overall voxel grid, in voxel units (a cube)', default = 32 )
    parser.add_argument('--margin', help = 'the margin (in angstroms) by which pdbs must fit inside the overall voxel grid', default = 0.5)
#the siamese network compares two inputs at a time:
    parser.add_argument('--number_channels', help = 'how many channels do we need', default = 1)
    parser.add_argument('--large_background', help ='Dkstance to use for null contacts, default is zero', default = False) 
    parser.add_argument('--loss', help = 'loss function', default='mse')
   
    args = parser.parse_args()
    loss = args.loss
    data_dir = args.data_directory
    key = args.key
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    n_aug = 3 #int(args.n_aug)
    learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    n_filters = int(args.n_filters)
    aug = args.augment
    check_size = args.check_size
    check_margin = args.margin
    vox_size = args.vox_size
    dims = args.dims
    dims = (dims,dims,dims)
    large_background = args.large_background
    n_channels = args.number_channels
   
    filters = n_filters
    dp = dropout #hp.Float('dropout', min_value=0.1, max_value=0.9, step=0.2)
    max_pool = True

#set up so that there are at least two blocks in each half of the U and then the middle layer
#any additional blocks are generated by the loops
    S_in = Input(shape=(32,32,32,8), name='S_in_shared')
    original_filters = filters   
#make the Siamese_Branch model - based on ResNet
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same', data_format="channels_last")(S_in)
    print(S.shape)
    S_pool = MaxPooling3D(padding = "same", data_format="channels_last")(S)
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same')(S_pool)
    S_add = Add()([S_pool, S])
    #S_add = Dropout(dp)(S_add)
#block one 
    Shortcut = Conv3D(filters =2*n_filters, kernel_size = 1, padding='same')(S_add)
    S = Conv3D(filters =2* n_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
    S = Conv3D(filters = 2*n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    #S_add = Dropout(dp)(S_add)
    
    Shortcut = Conv3D(filters =4*n_filters, kernel_size = 1, padding='same')(S_add)
    S = Conv3D(filters = 4*n_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
    S = Conv3D(filters = 4*n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    #S_add = Dropout(dp)(S_add)
    
#block 2
    Shortcut = Conv3D(filters = 8*n_filters, kernel_size = 1, padding='same')(S_add)
    S = Conv3D(filters = 8*n_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
    S = Conv3D(filters = 8*n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    #S_add = Dropout(dp)(S_add)
#block 3 

    S_out=S_add

    Siamese_Branch = Model(inputs=[S_in],outputs=[S_out])
    Siamese_Branch.summary()
    
    struct_a_input = Input(shape=(32,32,32,8), name='Struct_a_in')
    struct_b_input = Input(shape=(32,32,32,8), name='Struct_b_in')
    #make 1st copy of the branch submodel we delated above, using struct_a_input
    struct_a_branch = Siamese_Branch(struct_a_input)
    #make 2nd copy of the branch submodel we delated above, using struct_b_input
    struct_b_branch = Siamese_Branch(struct_b_input)


    #main model starts here
    main_input_layer = Add()([struct_a_branch, struct_b_branch])

    Shortcut = Conv3D(filters = n_filters, kernel_size = 1, padding='same')(main_input_layer)
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same')(main_input_layer)
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    #S_add = Dropout(dp)(S_add)
#block 4
    Shortcut = Conv3D(filters = 2*n_filters, kernel_size = 1, padding='same')(S_add)
    S = Conv3D(filters = 2*n_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
    S = Conv3D(filters = 2*n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([Shortcut, S])
    #S_add = Dropout(dp)(S_add)
#block 5
    #Shortcut = Conv3D(filters = 64*n_filters, kernel_size = 1, padding='same')(S_add)
    #S = Conv3D(filters = 64*n_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
    #S = Conv3D(filters = 64*n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    #S_add = Add()([Shortcut, S])
    #S_add = Dropout(dp)(S_add)
    #Reshape here so that we can apply 2D convolutions
    #what dimensions 
    #main_input_layer = Conv3DTranspose(filters=16, kernel_size=2, strides=2, activation='relu', padding='same')(main_input_layer)
    
    main_input_layer_shape = np.array(S_add.shape[1:])
    z = int(np.prod(main_input_layer_shape)/512/512)
    print(z)
    S = Reshape((512, 512, z))(S_add)

    conv1 = Conv2D(filters=int(n_filters/4), kernel_size=3, activation='relu', padding="same")(S)
    conv1 = Conv2D(filters=int(n_filters/4), kernel_size=3, activation='relu', padding="same")(conv1)
    if max_pool:
        pool1 = MaxPooling2D((2, 2))(conv1)
    else: 
        pool1 = AveragePooling2D((2, 2))(conv1)
    
    pool1 = Dropout(dp)(pool1)
        
    #block two
    conv2 = Conv2D(filters=int(n_filters/2), kernel_size=3, activation='relu', padding="same")(pool1)
    conv2 = Conv2D(filters=int(n_filters/2), kernel_size=3, activation='relu', padding="same")(conv2)
            
    if max_pool:
        pool2 = MaxPooling2D((2, 2))(conv2)
    else:
        pool2 = AveragePooling2D((2, 2))(conv2)
    pool2 = Dropout(dp)(pool2)
        
    #block three
    conv3 = Conv2D(filters=n_filters, kernel_size=3, activation='relu', padding="same")(pool2)
    conv3 = Conv2D(filters=n_filters, kernel_size=3, activation='relu', padding="same")(conv3)
            
    if max_pool:
        pool3 = MaxPooling2D((2, 2))(conv3)
    else:
        pool3 = AveragePooling2D((2, 2))(conv3)
    pool3 = Dropout(dp)(pool3)

    #block four
    conv4 = Conv2D(filters=2*n_filters, kernel_size=3, activation='relu', padding="same")(pool3)
    conv4 = Conv2D(filters=2*n_filters, kernel_size=3, activation='relu', padding="same")(conv4)
            
    if max_pool:
        pool4 = MaxPooling2D((2, 2))(conv4)
    else:
        pool4 = AveragePooling2D((2, 2))(conv4)
    pool4 = Dropout(dp)(pool4)
                #save this to use in other half
            
    conv5 = Conv2D(filters=4*n_filters, kernel_size=3, activation='relu', padding="same")(pool4)
    conv5 = Conv2D(filters=4*n_filters, kernel_size=3, activation='relu', padding="same")(conv5)
    
    if max_pool:
        pool5 = MaxPooling2D((2, 2))(conv5)
    else:
        pool5 = AveragePooling2D((2, 2))(conv5)
    pool5 = Dropout(dp)(pool5)
                #save this to use in other half

    convm = Conv2D(filters=8*n_filters, kernel_size=3, activation='relu', padding="same")(pool5)
    convm = Conv2D(filters=8*n_filters, kernel_size=3, activation='relu', padding="same")(convm)

    deconv5 = Conv2DTranspose(filters=4*n_filters, kernel_size=3,strides=2, activation='relu', padding='same')(convm)
    uconv5 = concatenate([deconv5, conv5])
    uconv5 = Dropout(dp)(uconv5)
    uconv5 = Conv2D(filters=4*n_filters, kernel_size=3, activation='relu', padding='same')(uconv5)
    uconv5 = Conv2D(filters=4*n_filters, kernel_size=3, activation='relu', padding='same')(uconv5)

    deconv4 = Conv2DTranspose(filters=2*n_filters, kernel_size=3, strides=2, activation='relu', padding='same')(uconv5)
    uconv4 = concatenate([deconv4, conv4])
    uconv4 = Dropout(dp)(uconv4)
    uconv4 = Conv2D(filters=2*n_filters, kernel_size=3, activation='relu', padding='same')(uconv4)
    uconv4 = Conv2D(filters=2*n_filters, kernel_size=3, activation='relu', padding='same')(uconv4)
    
    deconv3 = Conv2DTranspose(filters=n_filters, kernel_size=3, strides=2,activation='relu', padding='same')(uconv4)
    uconv3 = concatenate([deconv3, conv3])
    uconv3 = Dropout(dp)(uconv3)
    uconv3 = Conv2D(filters=n_filters, kernel_size=3, activation='relu', padding='same')(uconv3)
    uconv3 = Conv2D(filters=n_filters, kernel_size=3, activation='relu', padding='same')(uconv3)

    deconv2 = Conv2DTranspose(filters=int(n_filters/2), kernel_size=3,strides=2,activation='relu', padding='same')(uconv3)
    uconv2 = concatenate([deconv2, conv2])
    uconv2 = Dropout(dp)(uconv2)
    uconv2 = Conv2D(filters=int(n_filters/2), kernel_size=3, activation='relu', padding='same')(uconv2)
    uconv2 = Conv2D(filters=int(n_filters/2), kernel_size=3, activation='relu', padding='same')(uconv2)

    deconv1 = Conv2DTranspose(filters=int(n_filters/4), kernel_size=3,strides=2, activation='relu', padding='same')(uconv2)
    uconv1 = concatenate([deconv1, conv1])
    uconv1 = Dropout(dp)(uconv1)
    uconv1 = Conv2D(filters=int(n_filters/4), kernel_size=3, activation='relu', padding='same')(uconv1)
    uconv1 = Conv2D(filters=int(n_filters/4), kernel_size=3, activation='relu', padding='same')(uconv1)

    output_layer = Conv2D(1, 1, padding="same", activation='linear')(uconv1)
    #Reshape again to fit (512, 1024):
    output_layer = Reshape((512, 512))(output_layer)
    Unet = Model(inputs=[struct_a_input, struct_b_input], outputs=[output_layer])
    adam = tf.optimizers.Adam(lr=learning_rate)
    if loss == 'weighted_mse':
        Unet.compile(optimizer=adam,loss=weighted_mse, metrics=['mse', 'mae', 'accuracy', binary_accuracy])
    elif loss == 'weighted_mae':
        Unet.compile(optimizer=adam, loss=weighted_mae, metrics=['mse', 'mae', 'accuracy', binary_accuracy])
    else:
        Unet.compile(optimizer=adam, loss=loss, metrics=['mse', 'mae', 'accuracy', binary_accuracy])

    Unet.summary()
    
    """
    if key is None:
        key = data_dir.rstrip('/*.pdb') + str('/*.csv')
        key = glob.glob(key)
        print(key)
        if len(key) > 1:
            raise ValueError("Found more than one potential key file. Consider specifying a key manually")
        key = key[0]
    """    
    time_stamp = datetime.datetime.now().strftime("%Y%m%d-%H%M%S-%f")
    infile = open('/home/sdebesai/predictingcontactmaps/Utils/train_data_list_size_filtered', 'rb')
    train_data_list = pickle.load(infile)
    infile.close() 
   
    #load the eval set 
    #infile = open('/home/sdebesai/predictingcontactmaps/Utils/eval_set', 'rb')
    #eval_set = pickle.load(infile)
    #infile.close() 
    
    #load the train_test set
    #infile = open('/home/sdebesai/predictingcontactmaps/Utils/train_test', 'rb')
    #train_data_list = pickle.load(infile)
    #infile.close() 
    #25% of the data for testing
    np.random.seed(91106)
    full_len = len(train_data_list)
    eval_size = full_len // 4
    
    #to make test fold, set seed and shuffle in reproducible wayi
    test_size = full_len // 2
    
    #pick random indices for the test set
    #test_size = full_len // 4
    test_indicies = np.random.choice(np.array(range(full_len)),test_size,replace=False)
    test_indicies = sorted(test_indicies,reverse=True)
   
  
    test_fold = []
    
    eval_set = []

    #create test fold
    #for ind in test_indicies:
        #test_fold.append(train_data_list.pop(ind))
   
    for ind in test_indicies[:-eval_size]:
        test_fold.append(train_data_list.pop(ind))
        
    for ind in test_indicies[-eval_size:]:
        eval_set.append(train_data_list.pop(ind))

    subs = '2DQI_1'

    #locate file that we want to move to position 2
    remove_ind = 0
    for i, fn in enumerate(eval_set):
        if subs in fn[0]:
            remove_ind  = i 
    
    move_file = eval_set.pop(remove_ind)
    eval_set.insert(0, move_file)
    #print(eval_set[0])
    #print(eval_set[1])
    #print(eval_set[2])
    #with open('/central/scratch/sdebesai/logs/eval_sets/Unet/2DNew/test/' + time_stamp, 'w') as filehandle: 
        #filehandle.writelines("%s\n" % x for x in eval_set)

    print("Training set length: %i" %len(train_data_list))
    print("Test set length: %i" %len(test_fold))
    print("Eval set length: %i" %len(eval_set))
    train_data_list.extend(test_fold)
    train_streamer = Voxel_to_Contact_Generator(train_data_list, n_channels=n_channels, n_augmentations = 3, batch_size = 9, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)

    #test_streamer = Voxel_to_Contact_Generator(test_fold, n_channels=n_channels, n_augmentations = 3, batch_size = 9, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)

    eval_streamer = Voxel_to_Contact_Generator(eval_set, n_channels=n_channels, n_augmentations=3, batch_size=9, augment=aug, dim = dims, voxel_size=vox_size, margin=check_margin,shuffle=True,large_background=large_background, eval_set=True)
     

    x_eval, y = eval_streamer.__getitem__(0)
    #print(len(x))
    #print(len(y))
    num_images = 3
    #x_eval = [[x[0][0], x[0][3], x[0][6]], [x[1][0], x[1][3], x[1][6]]]
    y_eval = [y[0], y[3], y[6]]
    #TODO: scale distances!
    logdir_image = "/central/scratch/sdebesai/logs/image/Unet/2DFinal/final/" + time_stamp
    #logdir_image = "/home/sdebesai/predictingcontactmaps/Networks/Unet/2DNew/logs/image/test/" + time_stamp
    # Define the basic TensorBoard callback.
    #tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir_image)
    
    def log_image(epoch, logs):
      
        file_writer = tf.summary.create_file_writer(logdir_image + '/')
      # Use the model to predict the values from the validation dataset.
        test_pred_raw = Unet.predict(x_eval)
        test_pred_raw = [test_pred_raw[0], test_pred_raw[3], test_pred_raw[6]]
        with file_writer.as_default():
            images = colorize(test_pred_raw)
            binary_images = np.reshape(test_pred_raw, (-1, 512, 512, 1))
            binary_images = np.where(binary_images < 8.0, 1, 0)
            tf.summary.image("3 training data examples", images, max_outputs=6, step=epoch)
            tf.summary.image("3 training data examples, binary", binary_images, max_outputs=6, step=epoch)
            images = colorize(y_eval) 
            binary_images = np.reshape(y_eval, (-1, 512, 512, 1))
            binary_images = np.where(binary_images < 8.0, 1, 0)
            tf.summary.image("3 ground truth examples", images, max_outputs=6, step=epoch)
            tf.summary.image("3 ground truth examples, binary", binary_images, max_outputs=6, step=epoch)
          
    # Define the per-epoch callback
    IC = keras.callbacks.LambdaCallback(on_epoch_end=log_image)
    
    ES = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=10, verbose=1, mode='min', restore_best_weights = True)
    log_dir = '/central/scratch/sdebesai/logs/kerastuner/TB/Unet/2DFinal/final/' + time_stamp + '/'
    #log_dir = '/home/sdebesai/predictingcontactmaps/Networks/Unet/2DNew/logs/TB/test/' + time_stamp + '/'
    TB = TensorBoard(log_dir=log_dir, histogram_freq=1, embeddings_freq=1, write_graph=True, update_freq='batch')

               
    logfile = open(r'/central/scratch/sdebesai/logs/hyperparams/Unet/2DFinal/final/' + time_stamp, 'w')
    #logfile = open(r'/home/sdebesai/predictingcontactmaps/Networks/Unet/2DNew/logs/hyperparams/test/' + time_stamp, 'w')
    info = ['loss is: ' + loss + '\n', 'learning rate is: ' + str(learning_rate) + '\n', 'filters is: ' + str(n_filters) + '\n']
    logfile.writelines(info)
    logfile.close()
    
    class Metrics(tf.keras.callbacks.Callback):
        def __init__(self, val_data, batch_size=9):
            super().__init__()
            self.validation_data = val_data
            self.batch_size = batch_size
        def on_train_begin(self, logs={}):
            self.val_f1s = []
            self.val_recalls = []
            self.val_precisions = []
            self.val_NPVs = []
            self.val_specificities = []
        def on_epoch_end(self, epoch, logs={}):
            batches = len(self.validation_data)
            total = batches * self.batch_size
            val_pred = []
            val_true = []
            for batch in range(batches):
                xVal, yVal = self.validation_data.__getitem__(batch)
                val_true_batch = np.where(yVal < 8.0, 1, 0)
                val_pred_batch = np.asarray(self.model.predict(xVal))
                val_pred_batch = np.where(val_pred_batch < 8.0, 1, 0)
                val_pred.append(val_pred_batch.flatten())
                val_true.append(val_true_batch.flatten())
            val_pred = np.asarray(list(itertools.chain.from_iterable(val_pred)))
            val_true = np.asarray(list(itertools.chain.from_iterable(val_true)))
            val_pred.flatten()
            val_true.flatten()
            tn, fp, fn, tp = confusion_matrix(val_true, val_pred).ravel()
            _val_NPV = tn / (tn + fn)
            _val_specificity = tn / (tn + fp)
            _val_f1 = f1_score(val_true, val_pred)
            _val_precision = precision_score(val_true, val_pred)
            _val_recall = recall_score(val_true, val_pred)
            self.val_f1s.append(_val_f1)
            self.val_precisions.append(_val_precision)
            self.val_recalls.append(_val_recall)
            self.val_NPVs.append(_val_NPV)
            self.val_specificities.append(_val_specificity)
           
            print("-val_1: ",_val_f1, "- val_precision: ",_val_precision ,"- val_recall: ",_val_recall, "- val_NPV: ",_val_NPV, "- val_specificity: ", _val_specificity)
            filewriter = tf.summary.create_file_writer(log_dir + 'metrics')
            with filewriter.as_default():
                tf.summary.scalar('val_f1', data = _val_f1, step=epoch)
                tf.summary.scalar('val_recall', data = _val_recall, step=epoch)
                tf.summary.scalar('val_precision', data = _val_precision, step=epoch)
                tf.summary.scalar('val_NPV', data = _val_NPV, step=epoch)
                tf.summary.scalar('val_specificity', data = _val_specificity, step=epoch)
            return

    metrics = Metrics(val_data=eval_streamer)
    Unet.fit(train_streamer, validation_data=eval_streamer, epochs=epochs, shuffle=True, workers=1, max_queue_size=10, use_multiprocessing=False, callbacks=[ES, IC, TB, metrics])
    #Unet.fit(train_streamer, validation_data=test_streamer, epochs=epochs, shuffle=True, workers=1, max_queue_size=10, use_multiprocessing=False)

if __name__ == '__main__':
    main()


    
