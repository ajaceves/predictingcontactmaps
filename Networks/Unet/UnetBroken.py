'''
Unet V1. 2D convolutions. 
Hyperparams for tuning:
1. loss
2. optimizer
3. number of layers in branches
4. "height" of Unet (main layers)
5. learning rate
6. activation fxn
7. dropout 
8. using dropout
9. max/vs avg pooling
10. starting number of filters
'''
import glob, os, csv, random
from datetime import datetime
from copy import deepcopy

#Third party import
import argparse
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv3D, AveragePooling2D, Flatten, Dropout, Input, Dense, Reshape, Conv2D, Conv2DTranspose
from tensorflow.keras.layers import Activation, Lambda, Add, MaxPooling3D,MaxPooling2D, UpSampling2D, UpSampling3D
from tensorflow.keras.models import Model
from tensorflow.keras.losses import binary_crossentropy, MSE, MAE, MSLE, KLDivergence
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import SGD
import datetime
from tensorflow.keras.layers import Concatenate
import kerastuner as kt
from kerastuner.tuners import Hyperband
from kerastuner.engine.hypermodel import HyperModel
from kerastuner.engine.hyperparameters import HyperParameters
import IPython

#Custom modules
from predictingcontactmaps.Utils.KerasGenerator import Voxel_to_Contact_Generator

parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
parser.add_argument('--data_directory', type=str, help = 'directory to read structures from', default='../../Data/split_Ab_Ag_size_filtered')
parser.add_argument('--key', type=str, help = 'custom key, overwrites automatic detection from structure directory', default=None)
parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 2)
parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 12)
parser.add_argument('--n_aug', type=int, help = 'The number of augmentations to make to each piece of training data', default = 1)
parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0005)
parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.1)
parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 128)
parser.add_argument('--augment', help = 'Whether or not to augment data. If n_aug > 1, this is overwritten to True', default = True)
parser.add_argument('--check_size', help = 'Whether or not to check if all of the ligands fit within the voxel grids', default = True)
parser.add_argument('--vox_size', type=float, help = 'the size in angstroms of each voxel', default = 1.)
parser.add_argument('--dims', help = 'the length of one side of the overall voxel grid, in voxel units (a cube)', default = 32 )
parser.add_argument('--margin', help = 'the margin (in angstroms) by which pdbs must fit inside the overall voxel grid', default = 0.5)
#the siamese network compares two inputs at a time:
parser.add_argument('--number_channels', help = 'how many channels do we need', default = 1)
parser.add_argument('--large_background', help ='Dkstance to use for null contacts, default is zero', default = False)

args = parser.parse_args()
data_dir = args.data_directory
key = args.key
epochs = int(args.epochs)
batch_size = int(args.batch_size)
n_aug = int(args.n_aug)
learning_rate = float(args.learning_rate)
dropout = float(args.dropout)
n_filters = int(args.n_filters)
aug = args.augment
check_size = args.check_size
check_margin = args.margin
vox_size = args.vox_size
dims = args.dims
dims = (dims,dims,dims)
large_background = args.large_background
n_channels = args.number_channels
    
args = {'data_dir': data_dir, 'key':key, 'epochs':epochs, 'batch_size':batch_size, 'n_aug':n_aug, 'learning_rate':learning_rate,'dropout':dropout, 'n_filters':n_filters,'aug':aug, 'check_size':check_size,'check_margin':check_margin, 'vox_size':vox_size, 'dims':dims, 'n_channels':n_channels}

def build_model(hp):
    data_dir = args['data_dir']
    key = args['key']
    epochs=args['epochs']
    batch_size = args['batch_size']
    n_aug = args['n_aug']
    learning_rate = args['learning_rate']
    dropout = args['dropout']
    n_filters = args['n_filters']
    aug = args['aug']
    check_size = args['check_size']
    vox_size = args['vox_size']
    dims = args['dims']
    check_margin = args['check_margin']
    n_channels = args['n_channels']

    #obtain hparams:
    learning_rate_list = [4*learning_rate,2*learning_rate, learning_rate]
    loss_list = ['mse', 'mae', 'msle', 'kullback_leibler_divergence']
    optimizer_list = ['sgd', 'adam']
    activation_list = ['relu', 'sigmoid', 'tanh']    
    filters_list = [16, 32]#int(n_filters/4) , int(n_filters/2), n_filters]
    
    branch_layers = hp.Int('branch_layers', min_value=3, max_value=5)
    branch_layers=4
    main_layers = hp.Int('main_layers',min_value=0, max_value=5)
    main_layers=4
    activation = hp.Choice('activation', values=activation_list)
    #filters = hp.Choice('filters', values=filters_list)
    dp = hp.Float('dropout', min_value=0.1, max_value=0.9, step=0.1)
    include_dp = hp.Boolean('include dropout')
    max_pool = hp.Boolean('max pooling')
    filters=32
    struct_a_input = Input(shape=(32,32,32,8), name='Struct_a_in')
    struct_b_input = Input(shape=(32,32,32,8), name='Struct_b_in')

    #set up so that there are at least two blocks in each half of the U and then the middle layer
    #any additional blocks are generated by the loops
    S_in = Input(shape=(32,32,32,8), name='S_in_shared')
    original_filters = filters   
    #make the Siamese_Branch model - based on ResNet
    S = Conv3D(filters = filters, kernel_size = 2, activation=activation, padding='same', data_format="channels_last")(S_in)
    print(S.shape)
    S_pool = MaxPooling3D(padding = "same", data_format="channels_last")(S)
    S = Conv3D(filters = filters, kernel_size = 2, activation=activation, padding='same')(S_pool)
    S = Conv3D(filters = filters, kernel_size = 2, activation=activation, padding='same')(S)
    print(S.shape)
    S_add = Add()([S_pool, S])

    num_filters = filters
    for i in range(branch_layers):
        num_filters = num_filters*2
        Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation=activation, padding='same')(S_add)
        print(S.shape)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation=activation, padding='same')(S)
        print(S.shape)
        S_add = Add()([Shortcut, S])
    
    S_out=S_add

    Siamese_Branch = Model(inputs=[S_in],outputs=[S_out])
    Siamese_Branch.summary()
    
    #make 1st copy of the branch submodel we delated above, using struct_a_input
    struct_a_branch = Siamese_Branch(struct_a_input)
    #make 2nd copy of the branch submodel we delated above, using struct_b_input
    struct_b_branch = Siamese_Branch(struct_b_input)


    #main model starts here
    main_input_layer = Add()([struct_a_branch, struct_b_branch])
    filters=original_filters
    #Reshape here so that we can apply 2D convolutions
    #what dimensions? 
    main_input_layer_shape = np.array(main_input_layer.shape[1:])
    z = int(np.prod(main_input_layer_shape)/512/1024)
    print(z)
    main_input_layer = Reshape((512, 1024, z))(main_input_layer)

    conv1 = Conv2D(filters=original_filters, kernel_size=3, activation=activation, padding="same")(main_input_layer)
    conv1 = Conv2D(filters=original_filters, kernel_size=3, activation=activation, padding="same")(conv1)
    if max_pool:
        pool1 = MaxPooling2D((2, 2))(conv1)
    else: 
        pool1 = AveragePooling2D((2, 2))(conv1)
    if include_dp== True:
        pool1 = Dropout(dp)(pool1)
    levels = []
    pool_layer = pool1
    filters=original_filters
    for d in range(main_layers):
        filters = 2 * filters
        first_conv = Conv2D(filters=filters, kernel_size=3, activation=activation, padding="same")(pool_layer)
        second_conv = Conv2D(filters=filters, kernel_size=3, activation=activation, padding="same")(first_conv)
            
        if d < main_layers - 1:
            if max_pool:
                pool_layer = MaxPooling2D((2, 2))(second_conv)
            else:
                pool_layer = AveragePooling2D((2, 2))(second_conv)
            if include_dp:
                pool_layer = Dropout(dp)(pool_layer)
                #save this to use in other half
        else:
            #middle 
            pool_layer = second_conv
            #save this to use in other half
        levels.append(second_conv)
        #print(second_conv.shape)
    
    #second half
    #if there is more than one level to the Unet, we need to connect the middle to the the first deconv layer and we can build the rest using a for loop
    if len(levels) > 1:
        prev = levels[-1]
        #print(prev.shape[3])
        #filters = levels[-2].shape[3]
        deconv = Conv2DTranspose(filters, kernel_size=3,strides=(2, 2), activation=activation, padding='same')(prev)
        #print(deconv.shape)
        #print(levels[-2].shape)
    
        uconv = Concatenate()([deconv, levels[-2]])
        if include_dp:
            uconv = Dropout(dp)(uconv)
        uconv = Conv2D(filters,kernel_size=3, activation=activation, padding="same")(uconv)
        uconv = Conv2D(filters,kernel_size=3, activation=activation, padding="same")(uconv)
        #for loop goes down the layers in reverse order
        #start at d=depth-3
        for d in range(main_layers-3, -1, -1):
            conv = levels[d]
            filters = conv.shape[3]
            deconv = Conv2DTranspose(filters, kernel_size=3, strides=(2,2), padding="same")(uconv)
            uconv = Concatenate()([deconv, conv])
            if include_dp:
                uconv = Dropout(dp)(uconv)
                print(uconv.shape)
            uconv = Conv2D(filters,kernel_size=3, activation=activation, padding="same")(uconv)
            uconv = Conv2D(filters,kernel_size=3, activation=activation, padding="same")(uconv)
                   
        #print(uconv.shape)
        deconv1 = Conv2DTranspose(original_filters, kernel_size=3, strides=(2, 2), padding="same")(uconv)
        #print(deconv1.shape)
        #print(conv1.shape)
        uconv1 = Concatenate()([deconv1, conv1])
        if include_dp:
            uconv1 = Dropout(dp)(uconv1)
        uconv1 = Conv2D(original_filters,kernel_size=3, activation=activation, padding="same")(uconv1)
        uconv1 = Conv2D(original_filters, kernel_size=3, activation=activation, padding="same")(uconv1)
    #else, we connect the middle to the first layer
    else:
        prev = levels[-1]
        deconv1 = Conv2DTranspose(original_filters, kernel_size=3, strides=(2, 2), padding='same')(prev)
        uconv1 = Concatenate()([deconv1, conv1])
        if include_dp:
            uconv1 = Dropout(dp)(uconv1)
        uconv1 = Conv2D(original_filters,kernel_size=3, activation=activation, padding="same")(uconv1)
        uconv1 = Conv2D(original_filters, kernel_size=3, activation=activation, padding="same")(uconv1)
         
    output_layer = Conv2D(1, 1, padding="same", activation="sigmoid")(uconv1)
    #Reshape again to fit (512, 1024):
    output_layer = Reshape((512, 1024))(output_layer)
    #lr = hp.Choice('learning_rate', values=learning_rate_list)
    lr = 0.0005
    #optimizer = hp.Choice('optimizer', values=optimizer_list)
    optimizer = 'adam'
    #loss = hp.Choice('loss', values=loss_list)
    loss = 'mse'
    if optimizer == 'adam':
        optimizer = tf.optimizers.Adam(learning_rate=lr)
    elif optimizer == 'sgd':
        optimizer = tf.optimizers.SGD(learning_rate=lr)
    else:
        raise ValueError("Unexpected optimizer name: %r" % (optimizer,))
    global Unet
    Unet = Model(inputs=[struct_a_input, struct_b_input], outputs=[output_layer])
    Unet.compile(optimizer=optimizer, loss=loss, metrics=['mse'])
    
    Unet.summary()
    return Unet 
    
def main():
    data_dir = args['data_dir']
    key = args['key']
    epochs=args['epochs']
    batch_size = args['batch_size']
    n_aug = args['n_aug']
    learning_rate = args['learning_rate']
    dropout = args['dropout']
    n_filters = args['n_filters']
    aug = args['aug']
    check_size = args['check_size']
    vox_size = args['vox_size']
    dims = args['dims']
    check_margin = args['check_margin']
    n_channels = args['n_channels']

    data_dir = data_dir.rstrip('/') + str('/*.pdb')
    data_list = glob.glob(data_dir)
    """
    if key is None:
        key = data_dir.rstrip('/*.pdb') + str('/*.csv')
        key = glob.glob(key)
        print(key)
        if len(key) > 1:
            raise ValueError("Found more than one potential key file. Consider specifying a key manually")
        key = key[0]
    """    
    antibodies = glob.glob(data_dir.rstrip('/')+str('/*Ab.pdb'))
    print("Found %i structures" %len(data_list))
    #print("Using key: " +str(key))
    antibodies.sort() 
    train_data_list = []
    for ab in data_list:
        ag = ab.replace('_Ag.pdb', '_Ab.pdb')
        both = ab.replace('split_Ab_Ag_size_filterd', 'merged_Ab_Ag')
        train_data_list.append([ab, ag, both])
    keys_not_found = len(data_list) - len(train_data_list)
    print('Excluded %d files which were not found in the key.' % int(keys_not_found))
    
    #25% of the data for testing
    np.random.seed(91106)
    full_len = len(train_data_list)
    eval_size = full_len // 4
    cutoff = 0
    
    #to make test fold, set seed and shuffle in reproducible wayi
    test_size = full_len // 2
    test_indicies = np.random.choice(np.array(range(full_len)),test_size,replace=False)
    test_indicies = sorted(test_indicies,reverse=True)
    
    test_fold = []
    
    eval_set = []

    for ind in test_indicies[:-eval_size]:
        test_fold.append(train_data_list.pop(ind))
        
    for ind in test_indicies[-eval_size:]:
        eval_set.append(train_data_list.pop(ind))
    print("Training set length: %i" %len(train_data_list))
    print("Test set length: %i" %len(test_fold))
    print("Eval set length: %i" %len(eval_set))
 
    train_streamer = Voxel_to_Contact_Generator(train_data_list, n_channels=n_channels, n_augmentations =n_aug, batch_size = 8, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)
    test_streamer = Voxel_to_Contact_Generator(test_fold, n_channels=n_channels, n_augmentations = n_aug, batch_size = 8, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)
    eval_streamer = Voxel_to_Contact_Generator(eval_set, n_channels=n_channels, n_augmentations=n_aug, batch_size=8, augment=aug, dim = dims, voxel_size=vox_size, margin=check_margin, large_background=large_background)
     
    global x_eval
    global y_eval

    x_eval, y_eval = eval_streamer.__getitem__(0)
    num_images = 3
    x_eval = x_eval[0:num_images]
    y_eval = y_eval[0:num_images]
    #TODO: scale distances!
    logdir_image = "/central/scratch/sdebesai/logs/image/Unet/broken/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    # Define the basic TensorBoard callback.
    #tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir_image)
    
    def log_image(epoch, logs):
      global x_eval
      global y_eval
      global Unet
      
      file_writer = tf.summary.create_file_writer(logdir_image + '/')
      # Use the model to predict the values from the validation dataset.
      test_pred_raw = Unet.predict(x_eval)

      with file_writer.as_default():
        images = np.reshape(test_pred_raw, (-1, 512, 1024, 1))
        #tf.summary.image("3 training data examples", images, max_outputs=6, step=epoch)
        images = np.reshape(y_eval, (-1, 512, 1024, 1))
        tf.summary.image("3 ground truth examples", images, max_outputs=6, step=epoch)
         
    # Define the per-epoch callback

    IC = keras.callbacks.LambdaCallback(on_epoch_end=log_image)
    
    ES = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=10, verbose=1, mode='min', restore_best_weights = True)
    log_dir = '/central/scratch/sdebesai/logs/kerastuner/TB/Unet/broken/' + datetime.datetime.now().strftime('%m%d%-%H%M') + '/'
    TB = TensorBoard(log_dir=log_dir, histogram_freq=1, embeddings_freq=1, write_graph=True, update_freq='batch')
    directory = '/central/scratch/sdebesai/logs/kerastuner/oracle/Unet/broken/'
    project_name = 'UnetV1' + datetime.datetime.now().strftime('%m%d-%H%%M') 
    tuner = kt.Hyperband(build_model, objective='mse', max_epochs=epochs, hyperband_iterations=2, directory=directory, project_name=project_name)
    #class ClearTrainingOutput(tf.keras.callbacks.Callback):
         #def on_train_end(*args, **kwargs):
              #IPython.display.clear_output(wait=true)

    tuner.search(train_streamer, validation_data = test_streamer, epochs=epochs, callbacks=[ES, TB, IC])
    tuner.search_space_summary()
    best_model = tuner.get_best_models(num_models=1)[0]
    best_hyper_parameters = tuner.get_best_hyperparameters(1)[0]
               
if __name__ == "__main__":
    main()



    
