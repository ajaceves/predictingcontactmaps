'''
GoogleNet V5
This GoogleNet variant is in 3D. Reshaping occurs at the end. Not shallow. No aux nets.
1. loss
2. optimizer
3. learning rate
4. number of branch layers
5. starting number of filters
6. use dropout
7. dropout rate
8. filter factor
'''
import glob, os, csv, random
from datetime import datetime
from copy import deepcopy
import pickle
#Third party import
import argparse
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv3D, AveragePooling2D, Flatten, Dropout, Input, Dense, Reshape, Conv2D, BatchNormalization, Concatenate
from tensorflow.keras.layers import Activation, Lambda, Add, MaxPooling3D,MaxPooling2D, UpSampling2D, UpSampling3D, AveragePooling3D
from tensorflow.keras.models import Model
from tensorflow.keras.losses import binary_crossentropy, MSE, MAE, MSLE, KLDivergence
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import SGD
import kerastuner as kt
from kerastuner.tuners import Hyperband
import IPython
from kerastuner.engine.hyperparameters import HyperParameters
from kerastuner.engine.hypermodel import HyperModel
import datetime
import itertools
from sklearn.metrics import f1_score, precision_score, recall_score, confusion_matrix
#Custom modules
from predictingcontactmaps.Utils.KerasGenerator3 import Voxel_to_Contact_Generator
from predictingcontactmaps.Utils.ColorizeGreyscaleContactMaps import colorize

def weights(a):
    cutoff = 8
    weight_contact = 10
    weight_no_contact = 1
    a = K.less(a, cutoff)
    a = K.cast_to_floatx(a)
    return (a*(weight_contact-weight_no_contact)) + weight_no_contact

def weighted_mse(y_true, y_pred):
    weights_array = weights(y_true)
    diff = K.square(y_pred-y_true)
    weighted_diff = tf.math.multiply(weights_array, diff)
    return K.mean(weighted_diff, axis=-1)

def weighted_mae(y_pred, y_true):
    weights_array = weights(y_true)
    diff = K.abs(y_pred-y_true)
    weighted_diff = tf.math.multiply(weights_array, diff)
    return K.mean(weighted_diff, axis=-1)

def toBinary(a): 
    cutoff = 8
    a = K.less(a, cutoff)
    a = K.cast_to_floatx(a)
    return a
def binary_accuracy(y_pred, y_true):
    y_true_binary = toBinary(y_true)
    y_pred_binary = toBinary(y_pred)
    return K.mean(K.equal(y_true_binary, K.round(y_pred_binary)), axis=-1)
     
def inception(input_layer, filters_list):
    conv1 = Conv2D(filters=filters_list[0], kernel_size=(1,1), strides=1, padding='same', activation='relu')(input_layer)

            # 1x1->3x3
    conv2 = Conv2D(filters=filters_list[1][0], kernel_size=(1,1), strides=1, padding='same', activation='relu')(input_layer)
    conv2 = Conv2D(filters=filters_list[1][1], kernel_size=(3,3), strides=1, padding='same', activation='relu')(conv2)
    
            # 1x1->5x5
    conv3 = Conv2D(filters=filters_list[2][0], kernel_size=(1,1), strides=1, padding='same', activation='relu')(input_layer)
    conv3 = Conv2D(filters=filters_list[2][1], kernel_size=(5,5), strides=1, padding='same', activation='relu')(conv3)

        # 3x3->1x1
    pool_layer = MaxPooling2D(pool_size=(3,3), strides=1, padding='same')(input_layer)
    conv4 = Conv2D(filters=filters_list[3], kernel_size=(1,1), strides=1, padding='same', activation='relu')(pool_layer)

    return Concatenate(axis=-1)([conv1,conv2,conv3,conv4])

def main():
    parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
    parser.add_argument('--data_directory', type=str, help = 'directory to read structures from', default='/home/sdebesai/predictingcontactmaps/Data/split_Ab_Ag_size_filtered')
    parser.add_argument('--key', type=str, help = 'custom key, overwrites automatic detection from structure directory', default=None)
    parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 100)
    parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 12)
    parser.add_argument('--n_aug', type=int, help = 'The number of augmentations to make to each piece of training data', default = 1)
    parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0005)
    parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.1)
    parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 128)
    parser.add_argument('--augment', help = 'Whether or not to augment data. If n_aug > 1, this is overwritten to True', default = True)
    parser.add_argument('--check_size', help = 'Whether or not to check if all of the ligands fit within the voxel grids', default = True)
    parser.add_argument('--vox_size', type=float, help = 'the size in angstroms of each voxel', default = 1.)
    parser.add_argument('--dims', help = 'the length of one side of the overall voxel grid, in voxel units (a cube)', default = 32 )
    parser.add_argument('--margin', help = 'the margin (in angstroms) by which pdbs must fit inside the overall voxel grid', default = 0.5)
#the siamese network compares two inputs at a time:
    parser.add_argument('--number_channels', help = 'how many channels do we need', default = 1)
    parser.add_argument('--large_background', help ='Dkstance to use for null contacts, default is zero', default = False)
    parser.add_argument('--loss', help='loss fxn', default='mse')
    parser.add_argument('--factor', help='filter factor', default=1)

    args = parser.parse_args()
    data_dir = args.data_directory
    key = args.key
    epochs = int(args.epochs)
    batch_size = int(args.batch_size)
    n_aug = 3 #int(args.n_aug)
    learning_rate = float(args.learning_rate)
    dropout = float(args.dropout)
    n_filters = int(args.n_filters)
    aug = args.augment
    check_size = args.check_size
    check_margin = args.margin
    vox_size = args.vox_size
    dims = args.dims
    dims = (dims,dims,dims)
    large_background = args.large_background
    n_channels = args.number_channels
    loss = args.loss
    factor = 1 #int(args.factor)
    
    branch_layers = 4

    S_in = Input(shape=(32,32,32,8), name='S_in_shared')
    
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same', data_format="channels_last")(S_in)
    S_pool = MaxPooling3D(padding = "same", data_format="channels_last")(S)
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same')(S_pool)
    S = Conv3D(filters = n_filters, kernel_size = 2, activation='relu', padding='same')(S)
    S_add = Add()([S_pool, S])

    num_filters = n_filters
    for i in range(branch_layers):
        num_filters = num_filters*2
        Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
        S_add = Add()([Shortcut, S])

    S_out=S_add
    Siamese_Branch = Model(inputs=[S_in],outputs=[S_out])
        #Print a summary of the branch to the console
    Siamese_Branch.summary()
        #for layers in Siamese_Branch.layers:
        #   print(Siamese_Branch.output_shape)
        
    struct_a_input = Input(shape=(32,32,32,8), name='Struct_a_in')
    struct_b_input = Input(shape=(32,32,32,8), name='Struct_b_in')
        #make 1st copy of the branch submodel we delated above, using struct_a_input
    struct_a_branch = Siamese_Branch(struct_a_input)
        #make 2nd copy of the branch submodel we delated above, using struct_b_input
    struct_b_branch = Siamese_Branch(struct_b_input)
    
        #start googlenet
    layer_in = Add()([struct_a_branch, struct_b_branch])
    shape = np.array(layer_in.shape[1:])
    z = int(np.prod(shape)/512/512)
    layer_in = Reshape((512, 512, z))(layer_in)
        # stage-1
    layer = Conv2D(filters=int(factor*32), kernel_size=(7,7), strides=2, padding='same', activation='relu')(layer_in)
    layer = MaxPooling2D(pool_size=(3,3), strides=2, padding='same')(layer)
    layer = BatchNormalization()(layer)

        # stage-2
    layer = Conv2D(filters=int(factor*32), kernel_size=(1,1), strides=1, padding='same', activation='relu')(layer)
    layer = Conv2D(filters=int(factor*96), kernel_size=(3,3), strides=1, padding='same', activation='relu')(layer)
    layer = BatchNormalization()(layer)
        #layer = MaxPooling3D(pool_size=(3,3,3), strides=2, padding='same')(layer)

        # stage-3
    layer = inception(layer, [int(factor*32),  (int(factor*48),int(factor*64)), (int(factor*8),int(factor*16)), int(factor*16)]) #3a
    layer = inception(layer, [int(factor*64), (int(factor*64),int(factor*96)), (int(factor*16),int(factor*48)),int(factor*32)]) #3b
        #layer = MaxPooling3D(pool_size=(3,3,3), strides=1, padding='same')(layer)
    
        # stage-4
    layer = inception(layer, [int(factor*96),  (int(factor*48),int(factor*104)),  (int(factor*8),int(factor*24)),int(factor*32)]) #4a
        #aux1  = auxiliary(layer, name='aux1', include_dropout=include_dropout, dropout=dp)
    layer = inception(layer, [int(factor*80), (int(factor*56),int(factor*112)),  (int(factor*12),int(factor*32)), int(factor*32)]) #4b
    layer = inception(layer, [int(factor*64), (int(factor*64),int(factor*128)),  (int(factor*12),int(factor*32)), int(factor*32)]) #4c
    #layer = inception(layer, [int(factor*56), (int(factor*72),int(factor*144)),  (int(factor*16),int(factor*32)), int(factor*32)]) #4d
        #aux2  = auxiliary(layer, include_dropout=include_dropout, dropout=dp, name='aux2')
    #layer = inception(layer, [int(factor*256), (int(factor*160),int(factor*320)), (int(factor*32),int(factor*128)), int(factor*128)]) #4e
        #layer = MaxPooling3D(pool_size=(3,3,3), strides=2, padding='same')(layer)
    
        # stage-5
    #layer = inception(layer, [int(factor*128), (int(factor*80),int(factor*160)), (int(factor*16),int(factor*64)), int(factor*64)]) #5a
    #layer = inception(layer, [int(factor*192), (int(factor*96),int(factor*192)), (int(factor*24),int(factor*64)), int(factor*64)]) #5b
    layer = AveragePooling2D(pool_size=(7,7), strides=1, padding='same')(layer)
    #layer = Conv3D(filters=512, kernel_size=1, strides=1, padding='same')(layer) 
    #layer = Conv3D(filters=1024, kernel_size=1, strides=1, padding='same')(layer) 
        # stage-6
        #reshape to fit output. layers are commented out bc this has been adapted for 2D regression
        #layer = Flatten()(layer)
    layer = Dropout(0.1)(layer)
        #layer = Dense(units=256, activation='linear')(layer)
        #main = Dense(units=CLASS_NUM, activation='softmax', name='main')(layer)
        #reshape to 2D
    layer_shape = np.array(layer.shape[1:])
    print(layer_shape)
    z = int(np.prod(layer_shape)/512/512)
    layer = Reshape((512, 512, z))(layer)
        #do some convolutions
    
    #Shortcut = Conv2D(filters=int(n_filters/2), kernel_size=1, activation='relu', padding='same')(layer)
    #S = Conv2D(filters=int(n_filters/2), kernel_size=3, activation='relu', padding='same')(layer)
    #S = Conv2D(filters=int(n_filters/2), kernel_size=3, activation='relu', padding='same')(S)
    #S_add = Add()([Shortcut, S])
    
    #Shortcut = Conv2D(filters=int(n_filters), kernel_size=1, activation='relu', padding='same')(layer)
    #S = Conv2D(filters=int(n_filters), kernel_size=3, activation='relu', padding='same')(layer)
    #S = Conv2D(filters=int(n_filters), kernel_size=3, activation='relu', padding='same')(S
    #S_add = Add()([Shortcut, S])
    main = Conv2D(1, 1, activation='linear')(layer)
    main = Reshape((512, 512), name='main')(main)
    googlenet = Model(inputs=[struct_a_input, struct_b_input], outputs=[main])
    googlenet.summary()
    
    adam = tf.optimizers.Adam(lr=learning_rate)

    if loss == 'weighted_mse':
        googlenet.compile(loss=weighted_mse, optimizer=adam, metrics=['mse', 'mae', 'accuracy', binary_accuracy])
    elif loss == 'weighted_mae':
        googlenet.compile(loss=weighted_mae, optimizer=adam, metrics=['mse', 'mae', 'accuracy', binary_accuracy])
    else:
        googlenet.compile(loss=loss, optimizer=adam, metrics=['mse', 'mae', 'accuracy', binary_accuracy])
    
    data_dir = data_dir.rstrip('/') + str('/*.pdb')
    data_list = glob.glob(data_dir)
    """
    if key is None:
        key = data_dir.rstrip('/*.pdb') + str('/*.csv')
        key = glob.glob(key)
        print(key)
        if len(key) > 1:
            raise ValueError("Found more than one potential key file. Consider specifying a key manually")
        key = key[0]
    """    
    #antibodies = glob.glob(data_dir.rstrip('/')+str('/*Ab.pdb'))
    #print("Found %i structures" %len(data_list))
    #print("Using key: " +str(key))
    #antibodies.sort() 
    #train_data_list = []
    #for ab in data_list:
        #ag = ab.replace('_Ag.pdb', '_Ab.pdb')
        #both = ab.replace('split_Ab_Ag_size_filterd', 'merged_Ab_Ag')
        #train_data_list.append([ab, ag, both])
    infile = open('/home/sdebesai/predictingcontactmaps/Utils/train_data_list_size_filtered', 'rb')
    train_data_list = pickle.load(infile)
    infile.close()
    keys_not_found = len(data_list) - len(train_data_list)
    print('Excluded %d files which were not found in the key.' % int(keys_not_found))
    
    #25% of the data for testing
    np.random.seed(91106)
    full_len = len(train_data_list)
    eval_size = full_len // 4
    cutoff = 0
    
    #to make test fold, set seed and shuffle in reproducible wayi
    test_size = full_len // 2
    test_indicies = np.random.choice(np.array(range(full_len)),test_size,replace=False)
    test_indicies = sorted(test_indicies,reverse=True)
    
    test_fold = []
    
    eval_set = []

    for ind in test_indicies[:-eval_size]:
        test_fold.append(train_data_list.pop(ind))
        
    for ind in test_indicies[-eval_size:]:
        eval_set.append(train_data_list.pop(ind))
    print("Training set length: %i" %len(train_data_list))
    print("Test set length: %i" %len(test_fold))
    print("Eval set length: %i" %len(eval_set))
    train_data_list.extend(test_fold)
    sub = '2DQI_1'
    remove_index = 0
    for ind, fn in enumerate(eval_set):
        if sub in fn[0]:
            remove_index = ind
    eval_set.insert(0, eval_set.pop(remove_index))

    train_streamer = Voxel_to_Contact_Generator(train_data_list, n_channels=n_channels, n_augmentations =n_aug, batch_size = 9, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)
    #test_streamer = Voxel_to_Contact_Generator(test_fold, n_channels=n_channels, n_augmentations = n_aug, batch_size = 9, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)
    eval_streamer = Voxel_to_Contact_Generator(eval_set, n_channels=n_channels, n_augmentations=n_aug, batch_size=9, augment=aug, dim = dims, voxel_size=vox_size, margin=check_margin, large_background=large_background, eval_set=True)
    
    x_eval, y = eval_streamer.__getitem__(0)
    #x_eval = [[x[0][0], x[0][3], x[0][6]], [x[1][0], x[1][3], x[1][6]]]
    y_eval = [y[0], y[3], y[6]]
    time_stamp = datetime.datetime.now().strftime('Y%m%d-%H%M%S-%f')
    ES = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=10, verbose=1, mode='min', restore_best_weights = True)
    log_dir = '/central/scratch/sdebesai/logs/kerastuner/TB/GoogleNet/2DFinal/shallow/final/'+ time_stamp + '/'
    #log_dir = '/home/sdebesai/predictingcontactmaps/Networks/GoogleNet/2D/logs/TB/shallow_test/'+ time_stamp + '/'
    TB = TensorBoard(log_dir=log_dir,histogram_freq=1, embeddings_freq=1, write_graph=True, update_freq='batch')
    #TODO: scale distances!
    logdir_image = "/central/scratch/sdebesai/logs/image/GoogleNet/2DFinal/shallow/final/" + time_stamp + '/'
    #logdir_image = "/home/sdebesai/predicting_contactmaps/Networks/GoogleNet/2D/logs/image/shallow_test/" + time_stamp + '/'
    # Define the basic TensorBoard callback.
    #tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir_image)
    
    def log_image(epoch, logs):
      # Use the model to predict the values from the validation dataset.
      test_pred_raw = googlenet.predict(x_eval)
      test_pred_raw = [test_pred_raw[0], test_pred_raw[3], test_pred_raw[6]]
      file_writer = tf.summary.create_file_writer(logdir_image + '/')

      with file_writer.as_default():
        images = colorize(test_pred_raw)
        binary_images = np.reshape(test_pred_raw, (-1, 512, 512, 1))
        binary_images = np.where(binary_images < 8.0,1, 0) 
        tf.summary.image("3 training data examples", images, max_outputs=6, step=epoch)
        tf.summary.image("3 training data examples, binary", binary_images, max_outputs=6, step=epoch)
        images = colorize(y_eval)
        binary_images = np.reshape(y_eval, (-1, 512, 512, 1))
        binary_images = np.where(binary_images < 8.0,1, 0) 
        tf.summary.image("3 ground truth examples", images, max_outputs=6, step=epoch)
        tf.summary.image("3 ground truth examples, binary", binary_images, max_outputs=6, step=epoch)
         
    # Define the per-epoch callback

    img_callback = keras.callbacks.LambdaCallback(on_epoch_end=log_image)
    
    info = ['learning rate is: ' + str(learning_rate) + '\n', 'loss function is: ' + loss + '\n', 'the factor is: ' + str(factor) + '\n', 'filters is: ' + str(n_filters) + '\n']
    logfile = open(r'/central/scratch/sdebesai/logs/hyperparams/GoogleNet/2DFinal/shallow/final/' + time_stamp, 'w')
    #logfile = open(r'/home/sdebesai/predictingcontactmaps/Networks/GoogleNet/2D/logs/hyperparams/shallow_test/' + time_stamp, 'w')
    logfile.writelines(info)
    logfile.close()    
    class Metrics(tf.keras.callbacks.Callback):
        def __init__(self, val_data, batch_size=9):
            super().__init__()
            self.validation_data = val_data
            self.batch_size = batch_size
        def on_train_begin(self, logs={}):
            self.val_f1s = []
            self.val_recalls = []
            self.val_precisions = []
            self.val_specificities = []
            self.val_NPVs = []

        def on_epoch_end(self, epoch, logs={}):
            batches = len(self.validation_data)
            total = batches * self.batch_size
            val_pred = []
            val_true = []
            for batch in range(batches):
                xVal, yVal = self.validation_data.__getitem__(batch)
                val_true_batch = np.where(yVal < 8.0, 1, 0)
                val_pred_batch = np.asarray(self.model.predict(xVal))
                val_pred_batch = np.where(val_pred_batch < 8.0, 1, 0)
                val_pred.append(val_pred_batch.flatten())
                val_true.append(val_true_batch.flatten())
            val_pred = np.asarray(list(itertools.chain.from_iterable(val_pred)))
            val_true = np.asarray(list(itertools.chain.from_iterable(val_true)))
            val_pred.flatten()
            val_true.flatten()
            tn, fp, fn, tp = confusion_matrix(val_true, val_pred).ravel()
            _val_f1 = f1_score(val_true, val_pred)
            _val_precision = precision_score(val_true, val_pred)
            _val_recall = recall_score(val_true, val_pred)
            _val_specificity = tn / (tn + fp)
            _val_NPV = tn / (tn + fn)
            self.val_f1s.append(_val_f1)                          
            self.val_recalls.append(_val_recall)                          
            self.val_precisions.append(_val_precision)
            self.val_specificities.append(_val_specificity)
            self.val_NPVs.append(_val_NPV)
            print("- val_f1: ", _val_f1, "- val_precision: ", _val_precision, "- val_recall: ", _val_recall, "_val_NPV: ", _val_NPV, "_val_specificity: ", _val_specificity) 
            filewriter = tf.summary.create_file_writer(log_dir + "metrics")
            with filewriter.as_default():
                tf.summary.scalar("val_f1", data=_val_f1, step=epoch)                         
                tf.summary.scalar("val_recall", data=_val_recall, step=epoch)                         
                tf.summary.scalar("val_precision", data=_val_precision, step=epoch)                         
                tf.summary.scalar("val_specificity", data=_val_specificity, step=epoch)                         
                tf.summary.scalar("val_NPV", data=_val_NPV, step=epoch)                         
            return 
    metrics = Metrics(val_data=eval_streamer)

    googlenet.fit(train_streamer, validation_data=eval_streamer, epochs=epochs, shuffle=True, use_multiprocessing=False,callbacks=[ES, TB, img_callback, metrics])
    #googlenet.fit(train_streamer, validation_data=test_streamer, epochs=epochs, shuffle=True, use_multiprocessing=False)
if __name__ == "__main__":
    main()

