'''
#GoogleNet V3 
this differs from the previous two networks because it retains the use of the auxilrary nets + only has 6 inception modules in the main net. 2D convolutions.
Hyperparamters:
1. loss
2. optimizer
3. learning rate
4. number of branch layers 
5. starting number of filters in branch
6. filter factor
7. use dropout
8. dropout rate
'''
import glob, os, csv, random
from datetime import datetime
from copy import deepcopy
import pickle
#Third party import
import argparse
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv3D, AveragePooling2D, Flatten, Dropout, Input, Dense, Reshape, Conv2D, BatchNormalization, Concatenate
from tensorflow.keras.layers import Activation, Lambda, Add, MaxPooling3D,MaxPooling2D, UpSampling2D, UpSampling3D
from tensorflow.keras.models import Model
from tensorflow.keras.losses import binary_crossentropy, MSE, MAE, MSLE, KLDivergence
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers import SGD
import datetime
import IPython
import kerastuner as kt
from kerastuner.tuners import Hyperband
from kerastuner.engine.hypermodel import HyperModel
from kerastuner.engine.hyperparameters import HyperParameters
import datetime
#Custom modules
from predictingcontactmaps.Utils.KerasGenerator import Voxel_to_Contact_Generator
from predictingcontactmaps.Utils.ColorizeGreyscaleContactMaps import colorize

parser = argparse.ArgumentParser(description = 'Read in arguments relevant to training')
parser.add_argument('--data_directory', type=str, help = 'directory to read structures from', default='/home/sdebesai/predictingcontactmaps/Data/split_Ab_Ag_size_filtered')
parser.add_argument('--key', type=str, help = 'custom key, overwrites automatic detection from structure directory', default=None)
parser.add_argument('--epochs', type=int, help = 'The Number of Epochs to Train For', default = 50)
parser.add_argument('--batch_size', type=str, help = 'Batch size for neural net', default = 12)
parser.add_argument('--n_aug', type=int, help = 'The number of augmentations to make to each piece of training data', default = 1)
parser.add_argument('--learning_rate', type=float, help = 'Initial Learning Rate', default = 0.0005)
parser.add_argument('--dropout', type=float, help = 'Dropout', default = 0.1)
parser.add_argument('--n_filters', type=int, help = 'The Number of Filters to Use', default = 128)
parser.add_argument('--augment', help = 'Whether or not to augment data. If n_aug > 1, this is overwritten to True', default = True)
parser.add_argument('--check_size', help = 'Whether or not to check if all of the ligands fit within the voxel grids', default = True)
parser.add_argument('--vox_size', type=float, help = 'the size in angstroms of each voxel', default = 1.)
parser.add_argument('--dims', help = 'the length of one side of the overall voxel grid, in voxel units (a cube)', default = 32 )
parser.add_argument('--margin', help = 'the margin (in angstroms) by which pdbs must fit inside the overall voxel grid', default = 0.5)
#the siamese network compares two inputs at a time:
parser.add_argument('--number_channels', help = 'how many channels do we need', default = 1)
parser.add_argument('--large_background', help ='Dkstance to use for null contacts, default is zero', default = False)

args = parser.parse_args()
data_dir = args.data_directory
key = args.key
epochs = int(args.epochs)
batch_size = int(args.batch_size)
n_aug = int(args.n_aug)
learning_rate = float(args.learning_rate)
dropout = float(args.dropout)
n_filters = int(args.n_filters)
aug = args.augment
check_size = args.check_size
check_margin = args.margin
vox_size = args.vox_size
dims = args.dims
dims = (dims,dims,dims)
large_background = args.large_background
n_channels = args.number_channels
    
args = {'data_dir': data_dir, 'key':key, 'epochs':epochs, 'batch_size':batch_size, 'n_aug':n_aug, 'learning_rate':learning_rate,'dropout':dropout, 'n_filters':n_filters,'aug':aug, 'check_size':check_size,'check_margin':check_margin, 'vox_size':vox_size, 'dims':dims, 'n_channels':n_channels}

class GoogleNet(HyperModel):
    def __init__(self, args=args):
        self.data_dir = args['data_dir']
        self.key = args['key']
        self.epochs=args['epochs']
        self.batch_size = args['batch_size']
        self.n_aug = args['n_aug']
        self.learning_rate = args['learning_rate']
        self.dropout = args['dropout']
        self.n_filters = args['n_filters']
        self.aug = args['aug']
        self.check_size = args['check_size']
        self.vox_size = args['vox_size']
        self.dims = args['dims']
        self.check_margin = args['check_margin']
        self.n_channels = args['n_channels']
        self.learning_rate_list = [10*self.learning_rate,4*self.learning_rate,2*self.learning_rate, self.learning_rate]
        self.loss_list = ['mse', 'mae', 'msle', 'kullback_leibler_divergence']
        self.optimizer_list = ['sgd', 'adam']
        self.activation_list = ['relu', 'sigmoid', 'tanh','linear','softmax']    
        self.filters_list = [int(self.n_filters/16), int(self.n_filters/8), int(self.n_filters/4), int(self.n_filters/2), self.n_filters]
        #main_layers_list = [0, 1, 2]
        self.branch_layers_list = [3, 4] #5
        self.dropout_list = [0.4]
        self.factor_list = [1, 2, 4]

    def build(self, hp):
        def inception(input_layer, filters_list):

            #1x1 
            conv1 = Conv2D(filters=filters_list[0], kernel_size=(1,1), strides=1, padding='same', activation='relu')(input_layer)

            # 1x1->3x3
            conv2 = Conv2D(filters=filters_list[1][0], kernel_size=(1,1), strides=1, padding='same', activation='relu')(input_layer)
            conv2 = Conv2D(filters=filters_list[1][1], kernel_size=(3,3), strides=1, padding='same', activation='relu')(conv2)
            # 1x1->3X3--> 3x3
            conv3 = Conv2D(filters=filters_list[2][0], kernel_size=(1,1), strides=1, padding='same', activation='relu')(input_layer)
            conv3 = Conv2D(filters=filters_list[2][1], kernel_size=(5,5), strides=1, padding='same', activation='relu')(conv3)

            # pool --> 1x1
            pool_layer = MaxPooling2D(pool_size=(3,3), strides=1, padding='same')(input_layer)
            conv4 = Conv2D(filters=filters_list[3], kernel_size=(1,1), strides=1, padding='same', activation='relu')(pool_layer)

            return Concatenate(axis=-1)([conv1,conv2,conv3,conv4])

        def auxiliary(layer, which_reg, pool_size=(5,5), activation='linear', dense=256, include_dropout=True, dropout=dropout, name=None):
            layer = AveragePooling2D(pool_size=pool_size, strides=2, padding='same')(layer)
            #tune filters here?
            #print('aux dims', layer.shape)
            layer = Conv2D(filters=2048, kernel_size=(1,1), strides=1, padding='same', activation='relu')(layer)
            if include_dropout:
                if which_reg == 'dropout':
                    layer = Dropout(dropout)(layer)
                elif which_reg == 'batch_norm':
                    layer = BatchNormalization()(layer)
            dims = np.array(layer.shape[1:])
            
            z = int(np.prod(dims)/512/1024)
            layer = Reshape((512, 1024, z))(layer)
            layer = Conv2D(filters=1, kernel_size=(1,1), strides=1, padding='same', activation='linear')(layer)
            layer = Reshape((512, 1024), name=name)(layer)
            #layer = Flatten()(layer)
            #layer = Dense(units=dense, activation=activation)(layer)
    
            #layer = Dense(units=CLASS_NUM, activation='softmax', name=name)(layer)
            return layer

        #obtain hparams:
        optimizer=hp.Choice(name='optimizer', values=self.optimizer_list)
        loss = hp.Choice(name='loss', values=self.loss_list)
        lr = hp.Choice(name='learning_rate', values=self.learning_rate_list)
        branch_layers = 5 #hp.Int(name='branch_layers', min_value = 3, max_value=5)    
        dp =0.1 #hp.Float(name='dropout', min_value=0.1, max_value=0.9, step=0.2)
        filters= hp.Choice(name='filters', values=self.filters_list)
        factor = hp.Choice(name='factor', values=self.factor_list)
        include_dropout =False #hp.Boolean('include_dropout')
        which_reg = 'boolean' #hp.Choice('which reg', values=['dropout', 'batch_norm'])
        activation = hp.Choice('activation', values=self.activation_list)

        #set up so that there are at least two blocks in each half of the U and then the middle layer
        #any additional blocks are generated by the loops
        S_in = Input(shape=(32,32,32,8), name='S_in_shared')
    
        S = Conv3D(filters = filters, kernel_size = 2, activation='relu', padding='same', data_format="channels_last")(S_in)
        S_pool = MaxPooling3D(padding = "same", data_format="channels_last")(S)
        S = Conv3D(filters = filters, kernel_size = 2, activation='relu', padding='same')(S_pool)
        S = Conv3D(filters = filters, kernel_size = 2, activation='relu', padding='same')(S)
        S_add = Add()([S_pool, S])
        if include_dropout:
            if which_reg == 'dropout':
                S_add = Dropout(dropout)(S_add)
            elif which_reg == 'batch_norm':
                S_add = BatchNormalization()(S_add)

        num_filters = filters
        for i in range(branch_layers):
            num_filters = num_filters*2
            Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
            S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
            S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
            S_add = Add()([Shortcut, S])
            if include_dropout:
                if which_reg == 'dropout':
                    S_add = Dropout(dropout)(S_add)
                elif which_reg == 'batch_norm':
                    S_add = BatchNormalization()(S_add)

        num_filters = num_filters*2
        Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
        S_add = Add()([Shortcut, S]) 
        
        num_filters = num_filters*2
        Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
        S_add = Add()([Shortcut, S])
        
        num_filters = num_filters*2
        Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
        S_add = Add()([Shortcut, S])

        num_filters = num_filters*2
        Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
        S_add = Add()([Shortcut, S])

        num_filters = num_filters*2
        Shortcut = Conv3D(filters = num_filters, kernel_size = 1, padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S_add)
        S = Conv3D(filters = num_filters, kernel_size = 2, activation='relu', padding='same')(S)
        S_add = Add()([Shortcut, S])

        S_out=S_add
        Siamese_Branch = Model(inputs=[S_in],outputs=[S_out])
        #Print a summary of the branch to the console
        Siamese_Branch.summary()
        #for layers in Siamese_Branch.layers:
        #   print(Siamese_Branch.output_shape)
        struct_a_input = Input(shape=(32,32,32,8), name='Struct_a_in')
        struct_b_input = Input(shape=(32,32,32,8), name='Struct_b_in')
        #make 1st copy of the branch submodel we delated above, using struct_a_input
        struct_a_branch = Siamese_Branch(struct_a_input)
        #make 2nd copy of the branch submodel we delated above, using struct_b_input
        struct_b_branch = Siamese_Branch(struct_b_input)
    
        #start googlenet
        layer_in = Add()([struct_a_branch, struct_b_branch])
       #Reshape
        dim = np.array(layer_in.shape[1:])
        size = np.prod(dim)
        layer_in = Reshape((512, 1024, int(size/512/1024)))(layer_in)
        print('reshape')
        # stage-1
        layer = Conv2D(filters=64, kernel_size=(7,7), strides=2, padding='same', activation='relu')(layer_in)
        layer = MaxPooling2D(pool_size=(3,3), strides=2, padding='same')(layer)
        layer = BatchNormalization()(layer)
        print('stage 1')
        # stage-2
        layer = Conv2D(filters=64, kernel_size=(1,1), strides=1, padding='same', activation='relu')(layer)
        layer = Conv2D(filters=192, kernel_size=(3,3), strides=1, padding='same', activation='relu')(layer)
        layer = BatchNormalization()(layer)
        layer = MaxPooling2D(pool_size=(3,3), strides=2, padding='same')(layer)
        print('stage 2')
        # stage-3
        layer = inception(layer, [int(factor*64),  (int(factor*96),int(factor*128)), (int(factor*16),int(factor*32)), int(factor*32)]) #3a
        #layer = inception(layer, [int(factor*128), (int(factor*128),int(factor*192)), (int(factor*32),int(factor*96)),int(factor*64)]) #3b
        layer = MaxPooling2D(pool_size=(3,3), strides=2, padding='same')(layer)
        # stage-4
        layer = inception(layer, [int(factor*192),  (int(factor*96),int(factor*208)),  (int(factor*16),int(factor*48)),int(factor*64)]) #4a
        aux1  = auxiliary(layer, which_reg=which_reg, name='aux1', dropout=dp, include_dropout=include_dropout, activation=activation)
        layer = inception(layer, [int(factor*160), (int(factor*112),int(factor*224)),  (int(factor*24),int(factor*64)), int(factor*64)]) #4b
        #layer = inception(layer, [int(factor*128), (int(factor*128),int(factor*256)),  (int(factor*24),int(factor*64)), int(factor*64)]) #4c
        layer = inception(layer, [int(factor*112), (int(factor*144),int(factor*288)),  (int(factor*32),int(factor*64)), int(factor*64)]) #4d
        print(layer.shape)
        aux2  = auxiliary(layer,which_reg=which_reg, name='aux2',include_dropout=include_dropout, dropout=dp, activation=activation)
        layer = inception(layer, [int(factor*256), (int(factor*160),int(factor*320)), (int(factor*32),int(factor*128)), int(factor*128)]) #4e
        layer = MaxPooling2D(pool_size=(3,3), strides=2, padding='same')(layer)
        print('stage 3')
        # stage-5
        layer = inception(layer, [int(factor*256), (int(factor*160),int(factor*320)), (int(factor*32),int(factor*128)), int(factor*128)]) #5a
        #layer = inception(layer, [int(factor*384), (int(factor*192),int(factor*384)), (int(factor*48),int(factor*128)), int(factor*128)]) #5b
        print(layer.shape)
        layer = AveragePooling2D(pool_size=(7,7), strides=1, padding='same')(layer)
        layer = Conv2D(filters=2048, kernel_size=1, padding='same', activation=activation)(layer) 
        print(layer.shape)
        # stage-6
        #reshape to fit output. layers are commented out bc this has been adapted for 2D regression
        #layer = Flatten()(layer)
        if include_dropout:
            if which_reg == 'dropout':
                layer = Dropout(dp)(layer)
            elif which_reg == 'batch_norm':
                layer = BatchNormalization()(layer)
        #layer = Dense(units=256, activation='linear')(layer)
        #main = Dense(units=CLASS_NUM, activation='softmax', name='main')(layer)
        dims = np.array(layer.shape[1:])
        z = int(np.prod(dims)/512/1024)
        print('final reshape')
        layer = Reshape((512, 1024, z))(layer)
        print('done')
    
        main = Conv2D(1, 1, activation='linear')(layer)
        main = Reshape((512, 1024), name='main')(main)
        global googlenet
        googlenet = Model(inputs=[struct_a_input, struct_b_input], outputs=[main, aux1, aux2])
        googlenet.summary()
        if optimizer == 'adam':
            optimizer = tf.optimizers.Adam(learning_rate=learning_rate)
        elif optimizer == 'sgd':
            optimizer = tf.optimizers.SGD(learning_rate=learning_rate)
        else:
            raise ValueError("Unexpected optimizer name: %r" % (optimizer,))

        googlenet.compile(loss=loss, 
                  loss_weights={'main': 1.0, 'aux1': 0.3, 'aux2': 0.3},
                  optimizer=optimizer, metrics=['accuracy'])
        return googlenet

def main(data_dir=data_dir):
    model = GoogleNet()
    
    ES = EarlyStopping(monitor='val_loss', min_delta=0.001, patience=10, verbose=1, mode='min', restore_best_weights = True)
    log_dir = '/central/scratch/sdebesai/logs/kerastuner/TB/GoogleNet/v3/'+datetime.datetime.now().strftime('%m%d-%H%M') + '/'
    TB = TensorBoard(log_dir=log_dir,histogram_freq=1, embeddings_freq=1, write_graph=True, update_freq='batch')
    directory = '/central/scratch/sdebesai/logs/kerastuner/oracle/GoogleNet/v3/'
    project_name = 'GoogleNetV3' + datetime.datetime.now().strftime('%m%d-%H%M') 
    tuner = Hyperband(model, max_epochs = epochs,objective='val_loss', hyperband_iterations=5, directory=directory, project_name=project_name)

    data_dir = data_dir.rstrip('/') + str('/*.pdb')
    data_list = glob.glob(data_dir)
    """
    if key is None:
        key = data_dir.rstrip('/*.pdb') + str('/*.csv')
        key = glob.glob(key)
        print(key)
        if len(key) > 1:
            raise ValueError("Found more than one potential key file. Consider specifying a key manually")
        key = key[0]
    """    
    #antibodies = glob.glob(data_dir.rstrip('/')+str('/*Ab.pdb'))
    print("Found %i structures" %len(data_list))
    #print("Using key: " +str(key))
    #antibodies.sort() 
    #train_data_list = []
    #for ab in data_list:
        #ag = ab.replace('_Ag.pdb', '_Ab.pdb')
        #both = ab.replace('split_Ab_Ag_size_filterd', 'merged_Ab_Ag')
        #train_data_list.append([ab, ag, both])
    
    infile = open('/home/sdebesai/predictingcontactmaps/Utils/train_data_list', 'rb')
    train_data_list = pickle.load(infile)
    infile.close()
    keys_not_found = len(data_list) - len(train_data_list)
    print('Excluded %d files which were not found in the key.' % int(keys_not_found))
    
    #25% of the data for testing
    np.random.seed(91106)
    full_len = len(train_data_list)
    eval_size = full_len // 4
    cutoff = 0
    
    #to make test fold, set seed and shuffle in reproducible wayi
    test_size = full_len // 2
    test_indicies = np.random.choice(np.array(range(full_len)),test_size,replace=False)
    test_indicies = sorted(test_indicies,reverse=True)
    
    test_fold = []
    
    eval_set = []

    for ind in test_indicies[:-eval_size]:
        test_fold.append(train_data_list.pop(ind))
        
    for ind in test_indicies[-eval_size:]:
        eval_set.append(train_data_list.pop(ind))
    print("Training set length: %i" %len(train_data_list))
    print("Test set length: %i" %len(test_fold))
    print("Eval set length: %i" %len(eval_set))
 
    train_streamer = Voxel_to_Contact_Generator(train_data_list, n_channels=n_channels, n_augmentations =n_aug, batch_size = 8, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)
    test_streamer = Voxel_to_Contact_Generator(test_fold, n_channels=n_channels, n_augmentations = n_aug, batch_size = 8, augment = aug, dim = dims, voxel_size = vox_size, margin = check_margin, large_background=large_background)
    eval_streamer = Voxel_to_Contact_Generator(eval_set, n_channels=n_channels, n_augmentations=n_aug, batch_size=8, augment=aug, dim = dims, voxel_size=vox_size, margin=check_margin, large_background=large_background)
    global x_eval
    global y_eval
    x_eval, y_eval = eval_streamer.__getitem__(0)
    num_images = 3
    x_eval = x_eval[0:num_images]
    y_eval = y_eval[0:num_images]
    #TODO: scale distances!
    logdir_image = "/central/scratch/sdebesai/logs/image/GoogleNet/v3/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    # Define the basic TensorBoard callback.
    #tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir_image)
    
    def log_image(epoch, logs):
      # Use the model to predict the values from the validation dataset.
      global x_eval
      global y_eval
      global googlenet
      test_pred_raw = googlenet.predict(x_eval)
      file_writer = tf.summary.create_file_writer(logdir_image + '/')
      with file_writer.as_default():
        images = colorize(test_pred_raw) #np.reshape(test_pred_raw, (-1, 512, 1024, 1))
        tf.summary.image("3 training data examples", images, max_outputs=6, step=epoch)
        images = colorize(y_eval) #np.reshape(y_eval, (-1, 512, 1024, 1))
        tf.summary.image("3 ground truth examples", images, max_outputs=6, step=epoch)
         
    # Define the per-epoch callback

    img_callback = keras.callbacks.LambdaCallback(on_epoch_end=log_image)
    #class ClearTrainingOutput(tf.keras.callback.Callbac):
        #def on_train_end**args, **kwargs):
            #Iython.display.clear_ouput(wait=True)
    
    tuner.search(train_streamer, validation_data=test_streamer, epochs=epochs, shuffle=True, callbacks=[ES, TB, img_callback])
    tuner.search_space_summary()
    best_model = tuner.get_best_models(num_model=1)[0]
    best_model.summar()

if __name__ == "__main__":
    main()

