import numpy
import math
import copy

rotation_matrices = {}

# How many distinct degrees we can choose to rotate about
ROTATION_DEGREE_RESOLUTION = 1

# Taken from https://stackoverflow.com/a/6802723
def rotation_matrix(axis, theta):
    #TODO: Change this to quaternian based rotations
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = numpy.asarray(axis)
    axis = axis / math.sqrt(numpy.dot(axis, axis))
    a = math.cos(theta / 2.0)
    b, c, d = -axis * math.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return numpy.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                        [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                        [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])


def rotate(coordinates, axis, degrees):

    axis_degree_tuple = (tuple(axis), degrees)

    theta = numpy.deg2rad(degrees)

    if axis_degree_tuple not in rotation_matrices:

        rotation_matrices[axis_degree_tuple] = rotation_matrix(axis, theta)

    return numpy.dot(rotation_matrices[axis_degree_tuple], coordinates.T).T


def convert_categorical_to_binary(features, categories):
    category_map = {}
    category_index = 0

    for category in categories:
    #for category in sorted(categories):
        category_map[category] = category_index
        category_index += 1

    num_categories = category_index
    num_examples = len(features)
    binary_feature_matrix = numpy.zeros((num_examples, num_categories), dtype=numpy.int8)

    for example_index, feature in enumerate(features):
        feature_index = category_map[feature]
        binary_feature_matrix[example_index, feature_index] = 1

    return category_map, binary_feature_matrix


def voxelize(coordinate_list, features, center=(0, 0, 0), voxel_size=1, grid_shape=(20, 20, 20), empty_channel = False):

    # If features is an array, we are going to make a 4D tensor. 
    # Otherwise we will make a 3D tensor (we'll probably never use a 3D tensor...)
    if type(features[0]) is numpy.ndarray:
        if empty_channel:
            #adds channel for empty
            tensor_size = grid_shape + (len(features[0])+1,)
        else:
            tensor_size = grid_shape + (len(features[0]),)
    else:
        tensor_size = grid_shape
   
    num_dimensions = coordinate_list.shape[1]
    coordinate_list = numpy.subtract(coordinate_list, center)

    #create our empty 4D hypercube of zeros
    voxel_grid = numpy.zeros(tensor_size)
    if empty_channel:
        #initialize empty position as one, if we map something in, change value to zero
        voxel_grid[:,:,:,0] = 1

    for atom_index, coordinates in enumerate(coordinate_list):

        x_index = int(round(coordinates[0] / voxel_size) + round(grid_shape[0] / 2))
        y_index = int(round(coordinates[1] / voxel_size) + round(grid_shape[1] / 2))
        z_index = int(round(coordinates[2] / voxel_size) + round(grid_shape[2] / 2))

        # RESUME HERE
        # if > 20 or less than 0, 
        bad_indicies = []
        if x_index < 0 or y_index < 0 or z_index < 0 \
        or x_index >= grid_shape[0] or y_index >= grid_shape[1] or z_index >= grid_shape[2]:
            #print("passing")
            continue # this skips the rest of the loop and restarts at the beggining 

        if x_index < 0 or x_index >= grid_shape[0]:
            raise ValueError("Atom %i is out of x bounds" % atom_index)
        if y_index < 0 or y_index >= grid_shape[1]:
            raise ValueError("Atom %i is out of y bounds" % atom_index)
        if z_index < 0 or z_index >= grid_shape[2]:
            raise ValueError("Atom %i is out of z bounds" % atom_index)

        # if the coordinates of our atom are withing the voxelized cube,
        # put its features in the corresponding voxel
        if empty_channel:
            voxel_grid[x_index, y_index, z_index, 1:] = features[atom_index]
            voxel_grid[x_index, y_index, z_index, 0] = 0
        else:
            voxel_grid[x_index, y_index, z_index] = features[atom_index]
            #print(features[atom_index])
    return voxel_grid


def augment_dataset(coordinates, num_augmentations, center):
    '''WARNING, NEEDS TO BE PATCHED, OR WILL NOT RETURN ENOUGH DATA - JUNE 2020'''
    coordinates_array = numpy.empty((num_augmentations,) + coordinates.shape)
    centers_array = numpy.empty((num_augmentations,) + center.shape)

    for augmentation_index in range(num_augmentations):
        coordinates_rotated = copy.deepcopy(coordinates)

        rotation_degrees = numpy.random.randint(0, 359, 3)

        coordinates_rotated = rotate(
            coordinates_rotated, (1, 0, 0), rotation_degrees[0])
        coordinates_rotated = rotate(
            coordinates_rotated, (0, 1, 0), rotation_degrees[1])
        coordinates_rotated = rotate(
            coordinates_rotated, (0, 0, 1), rotation_degrees[2])

        coordinates_array[augmentation_index] = coordinates_rotated

        # modify center
        center_rotated = copy.deepcopy(center)

        center_rotated = rotate(
            center_rotated, (1, 0, 0), rotation_degrees[0])

        center_rotated = rotate(
            center_rotated, (0, 1, 0), rotation_degrees[1])

        center_rotated = rotate(
            center_rotated, (0, 0, 1), rotation_degrees[2])

        centers_array[augmentation_index] = center_rotated

    return coordinates_array, centers_array

def augment_predefined_angles(coordinates, rotation_list, center):

    coordinates_array = numpy.empty((len(rotation_list),) + coordinates.shape)
    centers_array = numpy.empty((len(rotation_list),) + center.shape)

    for augmentation_index in range(len(rotation_list)):
        coordinates_rotated = copy.deepcopy(coordinates)

        coordinates_rotated = rotate(
            coordinates_rotated, (1, 0, 0), rotation_list[augmentation_index][0])
        coordinates_rotated = rotate(
            coordinates_rotated, (0, 1, 0), rotation_list[augmentation_index][1])
        coordinates_rotated = rotate(
            coordinates_rotated, (0, 0, 1), rotation_list[augmentation_index][2])

        coordinates_array[augmentation_index] = coordinates_rotated

        # modify center
        center_rotated = copy.deepcopy(center)

        center_rotated = rotate(
            center_rotated, (1, 0, 0), rotation_list[augmentation_index][0])

        center_rotated = rotate(
            center_rotated, (0, 1, 0), rotation_list[augmentation_index][1])

        center_rotated = rotate(
            center_rotated, (0, 0, 1), rotation_list[augmentation_index][2])

        centers_array[augmentation_index] = center_rotated

    return coordinates_array, centers_array

