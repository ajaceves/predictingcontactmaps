import numpy as np
import time
from tensorflow import keras
import random
#custom libraries
from predictingcontactmaps.Utils.Featurizer import voxelize, convert_categorical_to_binary, augment_predefined_angles
from predictingcontactmaps.Utils.PDB_to_Contact import read_PDB_Simple, create_array
import sys
np.set_printoptions(threshold=sys.maxsize)

class Voxel_to_Contact_Generator(keras.utils.Sequence):
  #add augmentation

  #given an array of positions for atoms in a protein, calculate the center of the protein 
  def calc_center(self, input_array):
      array_center = np.mean((input_array.max(axis=0), input_array.min(axis=0)), axis = 0)
      return array_center

  def __init__(self, file_list, voxel_size, margin, n_augmentations, augment, batch_size=32, dim=(32, 32, 32), n_channels=1, shuffle=True, large_background = False, eval_set=False):
    'Initialization'
    self.dim = dim
    self.batch_size = batch_size
    self.shuffle = shuffle
    self.empty_channel = False
    self.n_augmentations = n_augmentations
    self.augment = augment
    #list of files: antibody, antigen, full complex
    self.file_list = file_list
    self.voxel_size = voxel_size
    self.atom_types = ['CA','C', 'H', 'O', 'N', 'P', 'S','Other']
    self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types,self.atom_types)
    self.n_channels = 1	
    self.large_background = False
    self.output_sizeX = 512
    self.output_sizeY = 512
    self.eval_set = eval_set
    #check that the number of augemnations is not greater than the batch_size
    if n_augmentations >  batch_size:
      raise ValueError('n_augmentations cannot be larger than the batch size!')

    #filter out proteins that are too big 
    #calculate distance constraint
    distance_constraint = dim[0] * voxel_size
    #initialize empty list with filtered out proteins 
    filtered_protein_list = []
    #iterate through each protein
    start = int(time.time())
    for filename in self.file_list:
        #gets positions for all atoms in each protein
        all_atoms_antibody = read_PDB_Simple(filename[0])[0]
        all_atoms_antigen = read_PDB_Simple(filename[1])[0]
        antibody_positions = np.array(all_atoms_antibody)[:, 0:3].astype(np.float)
        antigen_positions = np.array(all_atoms_antigen)[:, 0:3].astype(np.float)
        #find centers
        antibody_center = self.calc_center(antibody_positions)
        antigen_center = self.calc_center(antigen_positions)

        #find the atom furthest from the center of the antibody
        antibody_max_distance = np.linalg.norm((antibody_center - antibody_positions), axis = -1).max()
        antigen_max_distance = np.linalg.norm((antigen_center - antigen_positions), axis = -1).max()

        #Check that both are in range, if so, add to the filtered list
        if antibody_max_distance + margin  < distance_constraint and antigen_max_distance + margin  < distance_constraint:
          filtered_protein_list.append([filename[0], antibody_center, filename[1], antigen_center, filename[2]])
    runtime = int(time.time()) - start
    #print(f'Block 1 took {runtime} seconds')
    self.protein_list = filtered_protein_list
    self.on_epoch_end()
  def __len__(self):
    'Denotes the number of batches per epoch'
    return int(np.floor(len(self.protein_list * self.n_augmentations) / self.batch_size))

  def __getitem__(self, index):
    'Generate one batch of data'
    # Generate indexes of the batch
    indexes = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
    # Generate data
    [X0, X1], y = self.__data_generation(indexes)
    
    return [X0, X1], y

  def on_epoch_end(self):
    'Updates indexes after each epoch'
    self.indexes = np.arange(len(self.protein_list))
    if self.eval_set:
        if self.shuffle == True:
            copy = self.indexes[self.batch_size:]
            np.random.shuffle(copy)
            self.indexes[self.batch_size:] = copy
    else:
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

  def __data_generation(self, list_IDs):
    #start_batch = int(time.time())
    #not actually used currently
    cutoff = 0
    #will store positions of all the atoms on the antigens (voxelized)
    n_channels = len(self.atom_type_encoding)
    X0 = np.empty((self.batch_size, *self.dim, n_channels))
    #stores labels --> contact maps (initally empty)
    X1 = np.empty((self.batch_size, *self.dim, n_channels)) 
    y = []

    # Generate labels (contact maps)
    for index_num, ID in enumerate(list_IDs):
      #get the information for that protein at that index
      filename = self.protein_list[ID] #[filename[0], antibody_center, filename[1], antigen_center, filename[2]]
      #get the positions of just the CA atoms on the antibody and just the CA atoms on the antigen
      #print(filename)
      CA_antibody_data = read_PDB_Simple(filename[0])[1]
      CA_antigen_data = read_PDB_Simple(filename[2])[1]
     # print(np.array(CA_antibody_data).shape)
     # parsed_PDB = read_PDB_Simple("~\predictingcontactmaps\Summer2020Project\split_Ab_Ag_size_filtered\4K15_2_Ab.pdb")[0]
     # print(parsed_PDB)
      antibody_positions_CA = np.array(CA_antibody_data)[:, 0:3].astype(np.float)
      antigen_positions_CA = np.array(CA_antigen_data)[:, 0:3].astype(np.float)
      #get the positions of all the atoms on the antibody and just the CA atoms on the antigen
      all_antibody_data = read_PDB_Simple(filename[0])[0]
      all_antigen_data = read_PDB_Simple(filename[2])[0]
    
      antibody_positions_all = np.array(all_antibody_data)[:, 0:3].astype(np.float)
      antigen_positions_all = np.array(all_antigen_data)[:, 0:3].astype(np.float)
      
      #get the names of the atoms on the antibody and the antigen
      antibody_atoms = np.array(all_antibody_data)[:, 3].astype('str')
      antigen_atoms = np.array(all_antigen_data)[:, 3].astype('str')
      #one-hot encode atoms 
      antibody_atoms_hotcoded = np.empty((len(antibody_atoms),n_channels))
      for j, atom in enumerate(antibody_atoms):
        if atom not in self.atom_types:
          atom = 'Other'
        
        antibody_atoms_hotcoded[j,:] = self.atom_type_encoding[self.atom_type_key[atom]]
         
      antigen_atoms_hotcoded = np.empty((len(antigen_atoms),n_channels))
      for j, atom in enumerate(antigen_atoms):
        if atom not in self.atom_types:
          atom = 'Other'
        antigen_atoms_hotcoded[j,:] = self.atom_type_encoding[self.atom_type_key[atom]]
      #get the center
      antibody_center = filename[1]
      antigen_center =  filename[3]

      #create contact map array 
      contact_positions_temp = create_array(antibody_positions_CA, antigen_positions_CA, cutoff)
      
      #make empty array (batch size, x length of map, y length of map)
      if self.large_background:
          contact_positions = np.full([self.output_sizeX, self.output_sizeY], float(self.large_background))
      else:
          contact_positions = np.zeros([self.output_sizeX, self.output_sizeY])
      #insert contact_positions_temp into the top left corner
      contact_positions[:contact_positions_temp.shape[0],:contact_positions_temp.shape[1]] = contact_positions_temp
      #if filename[4] == '/home/sdebesai/predictingcontactmaps/Data/merged_Ab_Ag/1E4X_2.pdb':
          #print(contact_positions[0])
          #print(antigen_positions_all)  
      #check if data augmenting
      if self.augment or self.n_augmentations > 1:
        #augment data
        antigen_rotation_vectors = np.random.randint(0, 359, (self.n_augmentations,3))
        antibody_rotation_vectors = np.random.randint(0, 359, (self.n_augmentations,3))

        #augment all atoms + center
        antibody_augmented_positions, antibody_augmented_center = augment_predefined_angles(antibody_positions_all, antigen_rotation_vectors, antibody_center)
        antigen_augmented_positions, antigen_augemented_center = augment_predefined_angles(antigen_positions_all, antibody_rotation_vectors, antigen_center)
       
        #append voxelized location of the atoms
        for i in range(self.n_augmentations):
          #calcuate the augmented index
          aug_index = (index_num*self.n_augmentations) + i
          #voxelize
          X0[(aug_index),:] = voxelize(antibody_augmented_positions[i], antibody_atoms_hotcoded, center=antibody_augmented_center[i], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
          X1[(aug_index),:] = voxelize(antigen_augmented_positions[i], antigen_atoms_hotcoded, center=antigen_augemented_center[i], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)              
          #append contact map for each voxelized input
          y.append(contact_positions)
      else:
        #append voxelized location of the atoms
        X0[(index_num),:] = voxelize(antibody_positions_all, antibody_atoms_hotcoded, center=antibody_center, voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
        X1[(index_num),:] = voxelize(antigen_positions_all, antigen_atoms_hotcoded, center=antigen_center, voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)              
        #append contact map
        y.append(contact_positions)
    y = np.array(y)
    #runtime_batch = int(time.time()) - start_batch
    #print(f'Generating a batch of data took {runtime_batch} seconds')
    return [X0, X1], y
  
