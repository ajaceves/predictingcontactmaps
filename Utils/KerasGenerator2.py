import numpy as np
import time
from tensorflow import keras
import random
#custom libraries
from predictingcontactmaps.Utils.Featurizer import voxelize, convert_categorical_to_binary, augment_predefined_angles
from predictingcontactmaps.Utils.PDB_to_Contact import read_PDB_Simple, create_array
import sys
import pickle
np.set_printoptions(threshold=sys.maxsize)

class Voxel_to_Contact_Generator(keras.utils.Sequence):
  #add augmentation

  def __init__(self, file_list, voxel_size, margin, n_augmentations, augment, batch_size=32, dim=(32, 32, 32), n_channels=1, shuffle=True, large_background = False):
    'Initialization'
    self.dim = dim
    self.batch_size = batch_size
    self.shuffle = shuffle
    self.empty_channel = False
    self.n_augmentations = n_augmentations
    self.augment = augment
    #list of files: antibody, antigen, full complex
    self.file_list = file_list
    #print(self.file_list)
    self.voxel_size = voxel_size
    self.n_channels = 1
    self.atom_types = ['CA', 'C', 'H', 'O', 'N', 'P', 'S', 'Other']
    self.atom_type_key, self.atom_type_encoding = convert_categorical_to_binary(self.atom_types, self.atom_types)
    #unpickle the antibody_atoms_hotcoded
    infile_1 = open('/home/sdebesai/predictingcontactmaps/Utils/antibody_atoms_hotcoded_dict', 'rb')
    self.antibody_atoms_hotcoded_dict = pickle.load(infile_1)
    infile_1.close()
    #unpickle the antigen_atoms_hotcoded	
    infile_2 = open('/home/sdebesai/predictingcontactmaps/Utils/antigen_atoms_hotcoded_dict', 'rb')
    self.antigen_atoms_hotcoded_dict = pickle.load(infile_2)
    infile_2.close()
    #unpickle the contact map data
    infile_3 = open('/home/sdebesai/predictingcontactmaps/Utils/contact_maps_size_filtered','rb')
    self.contact_maps = pickle.load(infile_3)
    infile_3.close()
    #unpickle the antibody positions dict
    infile_4 = open('/home/sdebesai/predictingcontactmaps/Utils/antibody_positions_all_dict', 'rb')
    self.antibody_positions_all_dict = pickle.load(infile_4)
    infile_4.close()
    #unpickle the antigen positions dict
    infile_5 = open('/home/sdebesai/predictingcontactmaps/Utils/antigen_positions_all_dict', 'rb')
    self.antigen_positions_all_dict = pickle.load(infile_5)
    infile_5.close()
    #check that the number of augemnations is not greater than the batch_size
    if n_augmentations >  batch_size:
      raise ValueError('n_augmentations cannot be larger than the batch size!')

    #unpickle the filtered protein list 
    infile_6 = open('/home/sdebesai/predictingcontactmaps/Utils/filtered_protein_dict', 'rb')
    self.protein_list_dict = pickle.load(infile_6)
    infile_6.close()
    #print(self.protein_list_dict)
    #print(self.protein_list_dict)
    self.protein_list = []
    for filename in self.file_list:
        protein = filename[2]
        if protein in self.protein_list_dict:
            self.protein_list.append(self.protein_list_dict[protein])
    self.on_epoch_end()
  def __len__(self):
    'Denotes the number of batches per epoch'
    return int(np.floor(len(self.protein_list * self.n_augmentations) / self.batch_size))

  def __getitem__(self, index):
    'Generate one batch of data'
    # Generate indexes of the batch
    indexes = self.indexes[int(index*(self.batch_size/self.n_augmentations)):int((index+1)*(self.batch_size/self.n_augmentations))]
    # Generate data
    [X0, X1], y = self.__data_generation(indexes)
    return [X0, X1], y

  def on_epoch_end(self):
    'Updates indexes after each epoch'
    self.indexes = np.arange(len(self.protein_list))
    if self.shuffle == True:
      np.random.shuffle(self.indexes)

  def __data_generation(self, list_IDs):
    #start_batch = int(time.time())
    #not actually used currently
    cutoff = 0
    #will store positions of all the atoms on the antigens (voxelized)
    n_channels = len(self.atom_type_encoding)
    X0 = np.empty((self.batch_size, *self.dim, n_channels))
    #stores labels --> contact maps (initally empty)
    X1 = np.empty((self.batch_size, *self.dim, n_channels)) 
    y = []
    # Generate labels (contact maps)
    for index_num, ID in enumerate(list_IDs):
      #get the information for that protein at that index
      filename = self.protein_list[ID] #[filename[0], antibody_center, filename[1], antigen_center, filename[2]]
      merged_filename = filename[4]
      #get the associated contactmap
      contact_positions = self.contact_maps[merged_filename]
      print(contact_positions.shape)
 
      #if merged_filename == '/home/sdebesai/predictingcontactmaps/Data/merged_Ab_Ag/1E4X_2.pdb':
          #print(contact_positions[0])

      #get the positions of all the atoms on the antibody and just the CA atoms on the antigen
    
      antibody_positions_all = self.antibody_positions_all_dict[merged_filename]
      antigen_positions_all = self.antigen_positions_all_dict[merged_filename]
      
      #get one hotcoding
      antibody_atoms_hotcoded = self.antibody_atoms_hotcoded_dict[merged_filename] 
      antigen_atoms_hotcoded = self.antigen_atoms_hotcoded_dict[merged_filename]
      
      #get the center
      antibody_center = filename[1]
      antigen_center =  filename[3]

      #check if data augmenting
      if self.augment or self.n_augmentations > 1:
        #augment data
        antigen_rotation_vectors = np.random.randint(0, 359, (self.n_augmentations,3))
        antibody_rotation_vectors = np.random.randint(0, 359, (self.n_augmentations,3))

        #augment all atoms + center
        antibody_augmented_positions, antibody_augmented_center = augment_predefined_angles(antibody_positions_all, antigen_rotation_vectors, antibody_center)
        antigen_augmented_positions, antigen_augemented_center = augment_predefined_angles(antigen_positions_all, antibody_rotation_vectors, antigen_center)
       
        #append voxelized location of the atoms
        for i in range(self.n_augmentations):
          #calcuate the augmented index
          aug_index = (index_num*self.n_augmentations) + i
          #voxelize
          X0[(aug_index),:] = voxelize(antibody_augmented_positions[i], antibody_atoms_hotcoded, center=antibody_augmented_center[i], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
          X1[(aug_index),:] = voxelize(antigen_augmented_positions[i], antigen_atoms_hotcoded, center=antigen_augemented_center[i], voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)              
          #append contact map for each voxelized input
          print(X0[(aug_index),:].shape)
          y.append(contact_positions)
      else:
        #append voxelized location of the atoms
        X0[(index_num),:] = voxelize(antibody_positions_all, antibody_atoms_hotcoded, center=antibody_center, voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)
        X1[(index_num),:] = voxelize(antigen_positions_all, antigen_atoms_hotcoded, center=antigen_center, voxel_size = self.voxel_size, grid_shape=self.dim, empty_channel = self.empty_channel)              
        #append contact map
        y.append(contact_positions)
    y = np.array(y)
    #runtime_batch = int(time.time()) - start_batch
    #print(f'Generating a batch of data took {runtime_batch} seconds')
    return [X0, X1], y
  
