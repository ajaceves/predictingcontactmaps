import pickt the networks w binary contact maps e
from predictingcontactmaps.Utils.Featurizer import convert_categorical_to_binary
from predictingcontactmaps.Utils.PDB_to_Contact import read_PDB_Simple, create_array
import numpy as np
import sys
import glob, os, csv, random
from datetime import datetime
from copy import deepcopy

def calc_center(input_array):
    array_center = np.mean((input_array.max(axis=0), input_array.min(axis=0)), axis=0)
    return array_center

voxel_size = 1. 
dim = (32, 32, 32)
output_sizeX = 512
output_sizeY = 512
cutoff = 8
margin = 0.5 
distance_constraint = dim[0] * voxel_size
large_background = False
n_channels = 8
atom_types = ['CA', 'C', 'H', 'O', 'N', 'P', 'S', 'Other']
atom_type_key, atom_type_encoding = convert_categorical_to_binary(atom_types, atom_types)

antibodies = glob.glob('/home/sdebesai/predictingcontactmaps/Data/split_Ab_Ag_size_filtered/*Ab.pdb')
antibodies.sort()
train_data_list = []
#create list of filenames
for ab in antibodies:
    ag = ab.replace('_Ab.pdb', '_Ag.pdb')
    both = ab.replace('_Ab.pdb', '.pdb')
    both = both.replace('split_Ab_Ag_size_filtered', 'merged_Ab_Ag')
    #print(both) 
    train_data_list.append([ab, ag, both])
#pickle it!
#pickle_filename_1 = 'train_data_list'
#outfile_1 = open(pickle_filename_1, 'wb')
#pickle.dump(train_data_list, outfile_1)
#outfile_1.close()

train_data_list_2 = []

antibody_positions_all_dict = {}
antigen_positions_all_dict = {}
antibody_atoms_hotcoded_dict = {}
antigen_atoms_hotcoded_dict = {}
filtered_protein_dict = {}
for filename in train_data_list:
    #filename = [antibody, antigen, merged]
    merged_filename = filename[2]
    all_atoms_antibody = read_PDB_Simple(filename[0])[0]
    all_atoms_antigen = read_PDB_Simple(filename[1])[0]

    antibody_positions = np.array(all_atoms_antibody)[:, 0:3].astype(np.float)
    antigen_positions = np.array(all_atoms_antigen)[:, 0:3].astype(np.float)

    antibody_center = calc_center(antibody_positions)
    antigen_center = calc_center(antigen_positions)
    
    antibody_max_distance = np.linalg.norm((antibody_center-antibody_positions), axis=-1).max()
    antigen_max_distance = np.linalg.norm((antigen_center-antigen_positions), axis=-1).max()

    if antibody_max_distance + margin < distance_constraint and antigen_max_distance + margin < distance_constraint:
        #add position info to dict
        antibody_positions_all_dict.update({filename[2]:antibody_positions})
        antigen_positions_all_dict.update({filename[2]:antigen_positions})
        #hot-code
        antibody_atoms = np.array(all_atoms_antibody)[:, 3].astype('str')
        antigen_atoms =  np.array(all_atoms_antigen)[:, 3].astype('str')
        antibody_atoms_hotcoded = np.empty((len(antibody_atoms), n_channels))
        antigen_atoms_hotcoded = np.empty((len(antigen_atoms), n_channels))
        for j, atom in enumerate(antibody_atoms):
            if atom not in atom_types:
                atom = 'Other'
            antibody_atoms_hotcoded[j,:] = atom_type_encoding[atom_type_key[atom]]
        
        for j, atom in enumerate(antigen_atoms):
            if atom not in atom_types:
                atom = 'Other'
            antigen_atoms_hotcoded[j,:] = atom_type_encoding[atom_type_key[atom]]
        #add to the dicts to be pickled
        antibody_atoms_hotcoded_dict.update({merged_filename:antibody_atoms_hotcoded})
        antigen_atoms_hotcoded_dict.update({merged_filename: antigen_atoms_hotcoded})
        filtered_protein_dict.update({merged_filename:[filename[0], antibody_center, filename[1], antigen_center, filename[2]]})
        train_data_list_2.append(filename)        
#pickle filtered_protein_list
#pickle_filename_2 = 'filtered_protein_dict'
#outfile_2 = open(pickle_filename_2, 'wb')
#pickle.dump(filtered_protein_dict, outfile_2)
#outfile_2.close()
#pickle antibody_positions_all_dict
#pickle_filename_3 = 'antibody_positions_all_dict'
#outfile_3 = open(pickle_filename_3, 'wb')
#pickle.dump(antibody_positions_all_dict, outfile_3)
#outfile_3.close()
#pickle antigen_positions_all_dict
#pickle_filename_4 = 'antigen_positions_all_dict'
#outfile_4 = open(pickle_filename_4, 'wb')
#pickle.dump(antigen_positions_all_dict, outfile_4)
#outfile_4.close()
#pickle antibody_atoms_hotcoded_dict
#pickle_filename_5 = 'antibody_atoms_hotcoded_dict'
#outfile_5 = open(pickle_filename_5, 'wb')
#pickle.dump(antibody_atoms_hotcoded_dict, outfile_5)
#outfile_5.close()
#pickle antigen_atoms_hotcoded_dict
#pickle_filename_6 = 'antigen_atoms_hotcoded_dict'
#outfile_6 = open(pickle_filename_6, 'wb')
#pickle.dump(antigen_atoms_hotcoded_dict, outfile_6)
#outfile_6.close()
#create dictionary that maps filenames to contact maps
contact_maps = {}
#dictionary for contact maps meeting certain size threshold
contact_maps_size_filtered = {}
#list of ab/ag filenames for those where contact map is big enough
train_data_list_size_filtered = []
for filename in train_data_list_2:
    #filename = [antibody, antigen, merged]
    merged_filename = filename[2]
    CA_antibody_data = read_PDB_Simple(filename[0])[1]
    CA_antigen_data = read_PDB_Simple(filename[1])[1]

    antibody_positions_CA = np.array(CA_antibody_data)[:, 0:3].astype(np.float)
    antigen_positions_CA = np.array(CA_antigen_data)[:, 0:3].astype(np.float)

    contact_positions_temp = create_array(antibody_positions_CA, antigen_positions_CA, cutoff)

    if large_background:
        contact_positions = np.full([output_sizeX, output_sizeY], float(large_background))
    else:
        contact_positions = np.zeros([output_sizeX, output_sizeY])
    contact_positions[:contact_positions_temp.shape[0], :contact_positions_temp.shape[1]] = contact_positions_temp
    
    contact_maps.update({merged_filename:contact_positions})
    if antigen_positions_CA.shape[0] > 80:
        contact_maps_size_filtered.update({merged_filename:contact_positions})
        train_data_list_size_filtered.append(filename)

np.random.seed(91106)
full_len_all_data = len(train_data_list_2)
eval_size = full_len_all_data // 4
eval_indices = np.random.choice(np.array(range(full_len)), eval_size, replace=False)
eval_indices = sorted(eval_indices, reverse=True)
train_test = train_data_list_2
eval_set = []

full_len_big_maps = len(train_data_list_size_filtered)
eval_size_big = full_len_big_maps // 4
eval_indices_big = np.random.choice(np.array(range(full_len_big_maps)), eval_size_big, replace=False)
eval_indices_big = sorted(eval_indices_big, reverse=True)
train_test_big = train_data_list_size_filtered
eval_set_big = []

for ind in eval_indices:
    eval_set.append(train_test.pop(ind))

for ind in eval_indices_big:
    eval_set_big.append(train_test_big.pop(ind))

with open('eval_set.txt', 'w') as filehandle:
    filehandle.writelines("%s\n" % x for x in eval_set)

with open('eval_set_big.txt', 'w') as filehandle:
    filehandle.writelines("%s\n" % x for x in eval_set_big)

with open('train_test.txt', 'w') as filehandle:
    filehandle.writelines('%s\n' % x for x in train_test)

with open('train_test_big.txt', 'w') as filehandle:
    filehandle.writelines('%s\n' % x for x in train_test_big)


print(len(train_data_list_size_filtered))
#print(contact_maps['/home/sdebesai/predictingcontactmaps/Data/merged_Ab_Ag/1E4X_2.pdb'])
#pickle it!
#pickle_filename_7 = 'contact_maps'
#outfile_7 = open(pickle_filename_7, 'wb')
#pickle.dump(contact_maps, outfile_7)
#outfile_7.close()
pickle_filename_8 = 'contact_maps_size_filtered'
outfile_8 = open(pickle_filename_8, 'wb')
pickle.dump(contact_maps_size_filtered, outfile_8)
outfile_8.close()

pickle_filename_9 = 'train_data_list_size_filtered'
outfile_9 = open(pickle_filename_9, 'wb')
pickle.dump(train_data_list_size_filtered, outfile_9)
outfile_9.close()

pickle_filename_10 = 'eval_set'
outfile_10 = open(eval_set, 'wb')
pickle.dump(eval_set, outfile_10)
outfile_10.close()

pickle_filename_11 = 'eval_set_big'
outfile_11 = open(eval_set_big, 'wb')
pickle.dump(eval_set, outfile_11)
outfile_11.close()

pickle_filename_12 = 'train_test'
outfile_12 = open(train_test, 'wb')
pickle.dump(train_test, outfile_11)
outfile_12.close()

pickle_filename_12 = 'train_test_big'
outfile_12 = open(train_test_big, 'wb')
pickle.dump(train_test, outfile_11)
outfile_12.close()
