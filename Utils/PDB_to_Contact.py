import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import csv
import regex as re
import pandas as pd
import os

#This script contains functions that are used to generate contact maps from PDB files of protein complexes.

#modified this parser to output two lists of atoms. One only contains information for CA atoms since those are used in the contact maps
#the other contains info for all the atoms 
def read_PDB_Simple(file_name):
    atom_data = []
    atom_data_CA = []
    pattern = re.compile('^ATOM(.)*|^HETATM(.)*')
    sdf_descriptor = re.compile('^(.)*V2000')
    file_handle = open(file_name, 'r')
    index = 0
    for line in file_handle:
        if pattern.search(line):
            element = line[76:78].strip()
            residue_name = line[17:20].strip()
            chain = line[21].strip()
            residue_number = int(line[22:26].strip())
            insertion_code = line[26].strip()
            atom_name = line[12:16].strip()
            x_coord = float(line[30:38].strip())
            y_coord = float(line[38:46].strip())
            z_coord = float(line[46:54].strip()) 
            charge = line[78:90].strip()
            this_atom = [x_coord, y_coord, z_coord, element, index, charge, residue_name,
                        chain, residue_number, insertion_code]
            atom_data.append(this_atom)
           
            if atom_name == "CA":
                atom_data_CA.append(this_atom)
                index = index+1
    file_handle.close()
    return atom_data, atom_data_CA

#Parses PDB file and returns --> ended up not using in keras streamer
def read_PDB(filename, chain1, chain2):
    #chainA and chainB are a list of lists (positions of atoms on proteinA and proteinB)
    #variable name is misleading as each protein can have more than one chain, however, we just treat the chains on the same protein as continuous
    chainA = []
    chainB = []
    #contains information regarding atom name and which chain it is on
    labels = []
    pattern = re.compile('^ATOM(.)*|^HETATM(.)*')
    #open PDB file
    my_dir = '/mnt/c/Summer 2020 Project/merged_Ab_Ag'
    with open(filename+".pdb") as read_handle:
        #read each line using regex
        for line in read_handle:
            if pattern.search(line):
                residue_serial_number = int(line[22:26].strip())
                ll = list(line)
                #now slice up ll per the fields specified here: https://www.cgl.ucsf.edu/chimera/docs/UsersGuide/tutorials/framepdbintro.html (WARNING THIS NUMBERING STARTS AT 1, not 0!)
                atom_name = line[12:16].strip()
                #check if this is a C-alpha atom
                if atom_name == "CA":
                    #now get the chain this is on
                    chain = ll[21]
                    #check if the atom is on the first or second protein
                    #update the dict entry for whichever half of the protein
                    if chain in chain1:
                        #get all the position
                        x_coord = float(line[30:38].strip())
                        y_coord = float(line[38:46].strip())
                        z_coord = float(line[46:54].strip())
                        pos = [x_coord, y_coord, z_coord]
                        chainA.append(pos)
                        labels.append([atom_name, chain])
                    elif chain in chain2:
                        #get all the position
                        x_coord = float(line[30:38].strip())
                        y_coord = float(line[38:46].strip())
                        z_coord = float(line[46:54].strip())
                        pos = [x_coord, y_coord, z_coord]
                        chainB.append(pos)
                        labels.append([atom_name, chain])
    return chainA, chainB 

#calculates distance between two residues given their positions
def distance(resA, resB):
    #difference vector
    diff = np.array([x1 - x2 for (x1, x2) in zip(resA, resB)])
    #diff = np.array(resA) - np.array(resB)
    #distance is the L2 norm of the diff vector
    distance = np.linalg.norm(diff)
    return distance

#takes in cutoff dist + 2 1X3 arrays that contain the positions of the C-alpha atoms two residues
#returns whether the two are in contact or not
def in_contact(resA, resB, cutoff):
    dist = distance(resA, resB)
    contact = False
    if dist <= cutoff:
        contact = True

    return contact

#takes two chains that are part of the complex interface, as well as the cutoff distance and returns an array
#chain A and chain B are 3D np arrays (ith entry of each is the position of the C-alpha atom for that residue)
#the ijth entry of the array =1 if the distance between residue i (on chain A)
#and the distance between residue j (on chain B) <= cutoff distance, and 0 otherwise
def create_array(chainA, chainB, cutoff):
    #number of residues on each chain
    numResA = len(chainA)
    numResB = len(chainB)
    #array that we will return, initialize to be full of zeros
    array = np.zeros([numResA, numResB])
    
    #array is symmetric
    for i in range(numResA):
        resA = chainA[i]
        for j in range(numResB):
            resB = chainB[j]
            #if the chains are in contact change this location and symmetric one to 1
            #if in_contact(resA, resB, cutoff):
                #array[i][j]= 1
            array[i][j] = distance(resA, resB)
    return array

def create_array_binary(chainA, chainB, cutoff=8):
    #number of residues on each chain
    numResA = len(chainA)
    numResB = len(chainB)
    #array that we will return, initialize to be full of zeros
    array = np.zeros([numResA, numResB])
    
    #array is symmetric
    for i in range(numResA):
        resA = chainA[i]
        for j in range(numResB):
            resB = chainB[j]
            #if the chains are in contact change this location and symmetric one to 1
            if in_contact(resA, resB, cutoff):
                array[i][j]= 1
    return array
#takes in an array representing contacts at the interface and saves the 2D plot (contact map)
def visualize_map(contact_positions, filename):
    #contacts = pd.DataFrame(data=contact_positions)
    #sns.lmplot("ChainA", "ChainB", contacts,hue='Class', fit_reg=False)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #pos = np.where(contact_positions == 1)
    #ax.scatter(pos[0], pos[1], marker="+", c = 'black', linewidths=0)
    ax.imshow(contact_positions)
    plt.show()
    plt.savefig(filename+"ContactMap.png")

    return(filename+"ContactMap.png")

#just for testing, not used in streamer, only ran on one file to check
def main():
    #set cutoff distance
    cutoff = 20
    #need to generate potein1 and protein2 dict for ALL files

    
    #dict: filename:chains in protein 1
    protein1 = {'1A2Y_1':['H', 'L']}
    #dict: filename: chains in protein 2
    protein2 = {'1A2Y_1':['C']}
    filenames = ['1A2Y_1']
    #read PDB files
    path = "/mnt/c/Summer 2020 Project/merged_Ab_Ag"
    #for filename in os.listdir(path):
    for filename in filenames:
        chain1_list = protein1.get(filename)
        chain2_list = protein2.get(filename)
        print(chain1_list)
        #returns a list
        chainA, chainB = read_PDB(filename, chain1_list, chain2_list)
        contact_positions = create_array(chainA, chainB, cutoff)
        #create the contact map
        visualize_map(contact_positions, filename)


if __name__ == '__main__':
    main()
