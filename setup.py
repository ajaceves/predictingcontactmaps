#NOTE: to install this package, copy this setup file up one folder level and run "pip install -e ."
from setuptools import setup, find_packages
setup(
    name="predictingcontactmaps",
    version="0.1",
    author = "Serena Debesai, Aiden Aceves",
    author_email = "sdebesai@stanford.edu, ajaceves@gmail.com",
    packages=find_packages(),
)
